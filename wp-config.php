<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'alzi' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'I{)F$U#Uk6qG>s:k9S:wH*T4g*Tk}8 N=XP{L>0<YV{D3~~~J]S+0oY{fnsgcA|W' );
define( 'SECURE_AUTH_KEY',  '<Q4!% KSP:m`3Q36%^~Y:0H15C0Sj2= e>ap4fYjpe@B3om8cx%f;cy2vHSqC~<w' );
define( 'LOGGED_IN_KEY',    'xn3<L`W{ZCEST3z1ve^!jtEWekk<mt;7>8F#zt@#?OECFdSm|3,&$;ZXv!;###n9' );
define( 'NONCE_KEY',        'wJT}L;])XO[|bn[Wr(A?)VX5n{Me(q+%ZircmlrxQ>X{(n?6:`1:0:*[n!kgoW2-' );
define( 'AUTH_SALT',        'n!;s9WDP);upG$_VTlqZWxAB3Gk:&,H,s/^aR#i|xsAN=VT;oDU+VtS},N]25cZJ' );
define( 'SECURE_AUTH_SALT', 'VUX,~0Kb)D7DD033=P5U7S{;?V|Z(*v!lgnhLy*sgKh7?bzmmLYfk35=m_O4AU/?' );
define( 'LOGGED_IN_SALT',   '}, d*~(T?ksNVDEJ#~*8nUY%V-];)8kK}+iLR,>{=HSNx%l|/8d<zAt4] 2[ C0$' );
define( 'NONCE_SALT',       '2*CRtwR_l2bRP;N `Q(ah/h1?/WDn8cp3@uu=f#WWvzcmEia8k@7ZTY~Yxp:Xksw' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
