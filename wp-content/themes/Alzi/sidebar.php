
<div class="container mobileSayaAdalah" style="width: 100%">
        <div class="accordion col-12 " id="accordionExample">
            <h3>Saya Adalah ...</h3>
            <?php
            $args = array( 'post_type' => 'saya_adalah','order' => 'DESC','orderby'=>'title','posts_per_page'=>-1);
            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post();
                $setting = pods('saya_adalah',get_the_id());
                $featured_content = $setting->field('featured_content');
                $id = get_the_ID();
                //$bio = $setting->field('bio');
                ?>
            <div class="card" style="margin: 0;">
                <div class="card-header" id="headingTwo" style="background-color:#6f3a96;">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#<?php echo $id;?>" aria-expanded="true" aria-controls="collapseTwo" style="color:#fff;">
                            <?= the_title(); ?>
                            <i class="fa fa-chevron-down pull-right" color="#fff;" style=""></i>
                        </button>
                    </h2>
                </div>

                <div id="<?php echo $id;?>" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        <div>
                        <?php foreach ($featured_content as $row): ?>
                            <?php
                                $id = $row['ID'];
                            ?>
                            <li style="list-style: none;"><a href="<?=the_permalink($id)?>"><?= $row['post_title']; ?></a></li>
                        <?php endforeach ?>
                        </div>
                        <br>
                        <a href="<?=the_permalink()?>"><div class="pull-right" style="color:#542577">Detail <i class="fas fa-arrow-right " style="color:#542577;"></i></a>
                    </div><br>
                    </div>
                </div>
            </div>
            <?php
            endwhile;
            ?>
        </div>
</div>

<div class="card border-0" style="border-top:5px solid #6f3a96!important;">
    <div class="card-body" >
        <h4 style="margin-bottom: 20px;color:#6f3a96">Berita Terbaru</h4>
        <?php query_posts( array( 'Berita&Acara' => 'Berita&Acara', 'orderby' => 'ID', 'order' => 'DESC', 'posts_per_page' => -3 ) ); ?>
            <?php while ( have_posts() ) : the_post(); ?>
        <div class="media">
            <div class="media-body">
                <a href="<?php the_permalink();?>" style="font-size: 14px;color:#9756c7;"><?php the_title();?></a><br/>
                <small class="text-muted"><i class="fa fa-clock"></i> <?php the_time('D, d F Y'); ?></small>
            </div>
        </div>
        <?php endwhile; ?>
    <?php wp_reset_query(); ?>
    </div>
</div>

<div class="card border-0" style="border-top:5px solid #6f3a96!important;">
    <div class="card-body" >
        <h4 style="margin-bottom: 20px;color:#6f3a96">Kegiatan Terbaru</h4>
        <div class="row">
        <?php
        $args = array( 'post_type' => 'kegiatan','order' => 'asc','orderby'=>'title','posts_per_page'=>3);
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post();
        $setting = pods('kegiatan',get_the_id());
        $tgl = $setting->field('jadwal');
        $lks = $setting->field('lokasi');
        ?>
        <!--<div class="media">
            <div class="media-body">
                <a href="" style="color:#9756c7;font-size: 14px;"><?php //the_title(); ?></a><br/>
                <small class="text-muted"><i class="fa fa-clock"> <?php //date("D, d F Y", strtotime($tgl));?></i></small>
            </div>
        </div>-->
            <div class="media">
                <img src="<?= get_the_post_thumbnail_url(); ?>" class="mr-3 rounded" width="60" style="height: 60px; object-fit: cover; border: 2px solid #7A209B;">
                <div class="media-body">
                    <a href="<?php the_permalink();?>" style="font-size: 16px;color:#7A209B;"><?= the_title(); ?></a><br/>
                </div>
            </div><br/>
        <?php
        endwhile;
        ?>
    </div>
</div>
