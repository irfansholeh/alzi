 <?php 
/* Template Name: single */ 
get_header();
?>
<style type="text/css">

</style>
<div class="container" style=" padding-bottom: 120px; padding-top:50px;  margin-top: 90px;" >
    <div class="row">
        <?php while(have_posts()) : the_post(); ?>
		<div class="col-12 col-md-7 col-xl-7 col-sm-12"><h1 class="entry-title"><?php the_title(); ?></h1>
					<p class="post-meta text-muted" style="padding-top:15px;font-size:14px"> <?php the_time('d/m/Y'); ?>
                <!-- </span> |<a href="#" rel="category tag" style="">
                        <?php 
                        //echo "<pre>";
                        $kategori = get_the_category(get_the_ID()); 
                        foreach($kategori as $row) {
                            echo $row->name;
                        }?>
                </a></span> -->
            </p><article id="post-85" class="post-85 page type-page status-publish hentry">
					<div class="entry-content">
                    <?php
                        if (has_post_thumbnail()) {
                        the_post_thumbnail('post-thumbnail', ['class' => 'fto', 'title' => 'Feature image']);
                     }else{}?>
					<br><blockquote><p style="color:#000">
                    <?php the_content();?> 
                    <?php endwhile;?>
                    </p></blockquote>
				</div> <!-- .entry-content -->			
				</article> <!-- .et_pb_post -->
<br/><br/></div>
            				
<div class="col-lg-5 col-12 col-md-5" style="padding-left: 50px;">
    <?php get_template_part( 'sidebar' ); ?>
</div>
<style>
    
    .card {
    box-shadow: 0px 1px 2px 0px #e4e6e8;
    -webkit-box-shadow: 0px 1px 4px 0px #e4e6e8;
    -moz-box-shadow: 0px 1px 4px 0px #e4e6e8;
    margin:20px;
}
</style>
</div>
<br/><br/>

</div>
 </div>


<?php
get_footer();
?>