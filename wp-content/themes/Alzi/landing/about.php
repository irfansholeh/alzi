<section style="background: #fdb515; padding-top: 100px; padding-bottom: 100px">

    <div class="container">
        <div class="row">
            <div class="col-lg-8">

                <div class="row">
                    <div class="col-lg-7">
                        <p style="font-size: 15px; ">Alzheimer Indonesia which is also known as ALZI was established in Indonesia on August 3rd 2013. ALZI is supported by Alzheimer Nederland, Local Government DKI Jakarta, Ministry of Health, Ministry of Social Affairs, medias, private sectors, communities and volunteers of various ages and professionals including Neurologists, Psychiatrists, Geriatricians, Lawyers, Health Communication Specialists, Students, etc.</p>
                        <p style="font-size: 15px; ">Alzheimer Indonesia’s program focuses on advocacy, awareness raising, capacity building, research and development and strengthening internal organization.</p>
                    </p>
                    </div>
                    <div class="col-lg-5">
                        <img src="<?=base_url("asset/images/alzi-about.png")?>" width="100%"/><br/><br/>
                    </div>

                </div>

            </div>
            <div class="col-lg-4">

                <div class="card box-alzi" style="background: transparent">
                    <div class="card-body">
                        <h4 class="card-head-alzi">Chapters</h4>
                        <ul>
                            <li><a href="">Jakarta</a></li>
                            <li><a href="">Surabaya</a></li>
                            <li><a href="">Bandung</a></li>
                            <li><a href="">Yogyakarta</a></li>
                        </ul>
                        <a href="" class="btn btn-primary" style="margin-top: 10px; background: #7a219a; border-color: #7a219a; color:#ffffff">Lihat Semua</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

</section>