<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="margin-top: 90px;">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-none d-sm-block w-100" src="<?=base_url("asset/images/gradient-light.jpg")?>" alt="First slide">
            <img class="d-block d-sm-none w-100" src="<?=base_url("asset/images/slider-3-mobile.jpg")?>" alt="First slide">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
