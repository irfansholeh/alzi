<section style="background: #7a219a; padding-top: 100px; padding-bottom: 100px">

    <div class="container">
        <div class="row ">

            <div class="col-12 text-center">

                <h3 class="text-white"><span style="font-weight: 600">Mari berpartisipasi</span><br/><small>Untuk mengatasi Alzheimer & Demensia</small></h3><br/>

                <div class="row">
                    <div class="col-lg-4 col-12">
                        <a href="" style="border-radius: 30px; margin-bottom: 15px; font-size: 18px; padding-left: 100px; padding-right: 100px; border-color: #ffffff!important; color: #ffffff" class="btn btn-lg btn-outline-primary btn-cta-alzi btn-block"><i class="fa fa-dollar"></i> Berdonasi</a>
                    </div>
                    <div class="col-lg-4 col-12">
                        <a href="" style="border-radius: 30px; margin-bottom: 15px;  font-size: 18px; padding-left: 100px; padding-right: 100px; border-color: #ffffff!important; color: #ffffff" class="btn btn-lg btn-outline-primary btn-cta-alzi btn-block"><i class="fa fa-user"></i> Menjadi Relawan</a>

                    </div>
                    <div class="col-lg-4 col-12">
                        <a href="" style="border-radius: 30px; margin-bottom: 15px; font-size: 18px; padding-left: 100px; padding-right: 100px; border-color: #ffffff!important; color: #ffffff" class="btn btn-lg btn-outline-primary btn-cta-alzi btn-block"><i class="fa fa-calendar"></i> Ikut Kegiatan</a>

                    </div>
                </div>




            </div>

        </div>
    </div>
</section>