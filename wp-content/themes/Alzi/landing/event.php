<section style="background: #fcfcfc; padding-top: 100px; padding-bottom: 100px">

    <div class="container">
        <div class="row" style=" display: flex; flex-wrap: wrap;">

            <div class="col-12">
                <h3 style="font-weight: 600">Kegiatan</h3>
                <p>Berikut diantaranya kegiatan dari Alzi yang rutin diselenggarakan.</p><br/>
            </div>

            <div class="col-lg-4 col-12" style="display: flex;flex-direction: column;">
                <div class="card box-alzi" style="flex: 1;">
                    <img class="card-img-top" src="<?=base_url("asset/images/caregiver-1.jpg")?>" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title" style="font-weight: 600"><a href="">Pertemuan Caregiver</a></h5>
                        <p class="card-text" style="font-size: 14px">Kegiatan pertemuan yang diadakan Alzi setiap hari Sabtu di minggu pertama pada setiap bulannya</p>

                        <a class="badge" style="background-color:#fdb515;color:#000;">Mendatang</a><br/>
                        <a href="" style="font-size: 16px; color: #7A209B">Pertemuan Caregiver Maret 2019</a><br/>
                        <small><i class="fa fa-calendar"></i> Sabtu, 2 Maret 2019</small><br/>
                        <small><i class="fa fa-map-marker"></i> Pondok Indah</small>
                    </div>
                </div><br/>
            </div>

            <div class="col-lg-4 col-12" style="display: flex;flex-direction: column;">
                <div class="card box-alzi" style="flex: 1;">
                    <img class="card-img-top" src="<?=base_url("asset/images/world-alzi.jpg")?>" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title" style="font-weight: 600"><a href="">World Alzheimer’s Month<a/></h5>
                        <p class="card-text" style="font-size: 14px">Diperingati di Indonesia dengan berbagai acara di bulan September selama sebulan penuh</p>
                    </div>
                </div><br/>
            </div>

            <div class="col-lg-4 col-12" style="display: flex;flex-direction: column;">
                <div class="card box-alzi" style="flex: 1;">
                    <img class="card-img-top" src="<?=base_url("asset/images/remember-me.jpg")?>" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title" style="font-weight: 600"><a href="">Remember Me Charity Concert</a></h5>
                        <p class="card-text" style="font-size: 14px">Konser penggalangan dana untuk Alzheimer Indonesia</p>
                    </div>
                </div><br/>
            </div>

            <div class="col-12"><br/><br/>
                <a href="" class="btn btn-primary" style="background: #7a219a; border-color: #7a219a; color:#ffffff">Lihat Semua Kegiatan</a>
            </div>

        </div>
    </div>

</section>