<footer class="ftco-footer ftco-bg-dark ftco-section" style="padding-top: 100px">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h4 class="ftco-heading-2">Alzheimer Indonesia</h4>
                    <p>Alzheimer Indonesia is a non profit organization that aims to improve the quality life of people with Dementia / Alzheimer, their families and caregivers in Indonesia.</p>
                    <a href="#" class="fa fa-facebook ka"></a>
                    <a href="#" class="fa fa-youtube-play ka"></a>
                    <a href="#" class="fa fa-instagram ka"></a>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4 ml-md-5">
                    <h4 class="ftco-heading-2">Alzheimer dan Demensia</h4>
                    <ul class="list-unstyled">
                        <li><a href="#" class="py-2 d-block">Alzheimer</a></li>
                        <li><a href="#" class="py-2 d-block">Demensia</a></li>
                        <li><a href="#" class="py-2 d-block">Orang Dengan Demensia (ODD)</a></li>
                        <li><a href="#" class="py-2 d-block">10 Gejala Alzheimer</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h4 class="ftco-heading-2">Kegiatan</h4>
                    <ul class="list-unstyled">
                        <li><a href="#" class="py-2 d-block">Pertemuan Caregiver</a></li>
                        <li><a href="#" class="py-2 d-block">World Alzheimer’s Month</a></li>
                        <li><a href="#" class="py-2 d-block">Remember Me Charity Concert</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h4 class="ftco-heading-2">Memerlukan Bantuan?</h4>
                    <div class="block-23 mb-3">
                        <ul style="padding-left: 20px;">
                            <li><span class="text">Plaza 3 Blok E No. 2, Pondok Indah, Jakarta Selatan 12310</span></li>
                            <li><a href="#"><span class="text">+62 816 86 3136</span></a></li>
                            <li><a href="#"><span class="text">info@alzi.or.id</span></a></li>
                            <li><a href="#"><span class="text"><a href="https://www.facebook.com/AlzheimerIndonesia">Facebook Alzheimer Indonesia</a>
</span></a></li>
                            <li><a href="#"><span class="text"><a href="https://www.facebook.com/AlzheimerIndonesia">Twitter <a href="http://twitter.com/alzi_indonesia">@alzi_indonesia</a>
</a>
</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row" style="margin-top:10px;">
            <div class="col-md-12 text-center p-2" style="background-color:#290142;">

                <p style="padding-top:15px;padding-bottom:5px"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright ©<script>document.write(new Date().getFullYear());</script>2019 Yayasan Alzheimer Indonesia
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            </div>
        </div>
    </div>
</footer>