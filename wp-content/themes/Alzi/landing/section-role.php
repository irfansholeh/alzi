<section style="background:#fcfcfc; padding-top: 70px; padding-bottom: 70px;">

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3 style="font-weight: 600; margin-bottom: 20px;">Saya adalah..</h3>
            </div>
        </div>
    </div>

    <!--ffd6ff-->
    <div class="container d-none d-xl-block d-lg-block d-xl-none">
        <div class="row">

            <div class="col-lg-4 col-12">
                <div class="card box-alzi">
                    <div class="card-body">
                        <h4 class="card-head-alzi">Orang Demensia</h4>
                        <ul>
                            <li> <a href="">Bersiap akan perubahan</a></li>
                            <li> <a href="">Mengurus diri sendiri</a></li>
                            <li> <a href="">Mengemudi</a></li>
                        </ul>
                    </div>
                </div><br/>
            </div>

            <div class="col-lg-4 col-12">
                <div class="card box-alzi">
                    <div class="card-body">
                        <h4 class="card-head-alzi">Pendamping</h4>
                        <ul>
                            <li> <a href="">Tahap awal</a></li>
                            <li> <a href="">Gejala lanjutan</a></li>
                            <li> <a href="">Komunikasi</a></li>
                        </ul>
                    </div>
                </div><br/>
            </div>

            <div class="col-lg-4 col-12">
                <div class="card box-alzi">
                    <div class="card-body">
                        <h4 class="card-head-alzi">Mendukung</h4>
                        <ul class="list-unstyled">
                            <li> <a href="">Komunikasi</a></li>
                            <li> <a href="">Memahami perilaku</a></li>
                            <li> <a href="">Menekankan</a></li>
                        </ul>
                    </div>
                </div><br>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-4 col-12">
                <div class="card box-alzi">
                    <div class="card-body">
                        <h4 class="card-head-alzi">Mengurangi Resiko</h4>
                        <ul class="list-unstyled">
                            <li> <a href="">Faktor resiko</a></li>
                            <li> <a href="">kesehatan otak</a></li>
                            <li> <a href="">Genetika</a></li>
                        </ul>
                    </div>
                </div><br>
            </div>
            <div class="col-lg-4 col-12">
                <div class="card box-alzi">
                    <div class="card-body">
                        <h4 class="card-head-alzi">Peduli demensia</h4>
                        <ul class="list-unstyled">
                            <li> <a href="">Memahami permasalahan</a></li>
                            <li> <a href="">Mengatahui pengobatan</a></li>
                            <li> <a href="">Mencari tau solusi</a></li>
                        </ul>
                    </div>
                </div><br>
            </div>
            <div class="col-lg-4 col-12">
                <div class="card box-alzi">
                    <div class="card-body">
                        <h4 class="card-head-alzi">Pahami Alzheimer</h4>
                        <ul>
                            <li> <a href="">Alzheimer dan Pikun</a></li>
                            <li> <a href="">10 Gejala</a></li>
                            <li> <a href="">Alzheimer Indonesia</a></li>
                        </ul>
                    </div>
                </div><br/>
            </div>
        </div>
    </div>

    <!--Mobile only-->
    <div class="container mobileSayaAdalah d-block d-sm-none d-sm-block d-md-none d-md-block d-lg-none">
        <div class="accordion" id="accordionExample">

            <div class="card">
                <div class="card-header" id="headingTwo" style="background-color:#6f3a96;">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" style="color:#fff;">
                            Orang Demensia
                            <i class="fa fa-chevron-down pull-right" color="#fff;" style=""></i>

                        </button>
                    </h2>
                </div>

                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul class="list-unstyled">
                            <li> <a href="">Bersiap akan perubahan</a></li>
                            <li> <a href="">Mengurus diri sendiri</a></li>
                            <li> <a href="">Mengemudi</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="heading3" style="background-color:#6f3a96;">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3" style="color:#fff;">
                           Pendamping
                            <i class="fa fa-chevron-down pull-right" color="#fff;" style=""></i>
                        </button>
                    </h2>
                </div>

                <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul class="list-unstyled">
                            <li> <a href="">Tahap awal</a></li>
                            <li> <a href="">Gejala lanjutan</a></li>
                            <li> <a href="">Komunikasi</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="heading4" style="background-color:#6f3a96;">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4" style="color:#fff;">
                           Mendukung
                            <i class="fa fa-chevron-down pull-right" color="#fff;" style=""></i>
                        </button>
                    </h2>
                </div>

                <div id="collapse4" class="collapse " aria-labelledby="heading4" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul class="list-unstyled">
                            <li> <a href="">Memahami perilaku</a></li>
                            <li> <a href="">Menekankan</a></li>
                            <li> <a href="">Komunikasi</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="heading5" style="background-color:#6f3a96;">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse5" style="color:#fff;" aria-expanded="true" aria-controls="collapse5">
                            Mengurangi Resiko
                            <i class="fa fa-chevron-down pull-right" color="#fff;" style=""></i>
                        </button>
                    </h2>
                </div>

                <div id="collapse5" class="collapse " aria-labelledby="heading5" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul class="list-unstyled">
                            <li> <a href="">Faktor resiko</a></li>
                            <li> <a href="">kesehatan otak</a></li>
                            <li> <a href="">Genetika</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="heading6" style="background-color:#6f3a96;">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" style="color:#fff;" type="button" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6"><i class="fas fa-arrow-alt-circle-down" color="#fff;" style="margin-right:4px"></i>
                            Peduli demensia
                            <i class="fa fa-chevron-down pull-right" color="#fff;" style=""></i>
                        </button>
                    </h2>
                </div>

                <div id="collapse6" class="collapse " aria-labelledby="heading6" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul class="list-unstyled">
                            <li> <a href="">Memahami permasalahan</a></li>
                            <li> <a href="">Mengatahui pengobatan</a></li>
                            <li> <a href="">Mencari tau solusi</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingOne" style="background-color:#6f3a96;">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="color:#fff;">
                            Ingin Pahami Alzheimer
                            <i class="fa fa-chevron-down pull-right" color="#fff;" style=""></i>
                        </button>
                    </h2>
                </div>

                <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul class="list-unstyled">
                            <li> <a href="">Alzheimer dan Pikun</a></li>
                            <li> <a href="">10 Gejala</a></li>
                            <li> <a href="">Alzheimer Indonesia</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>