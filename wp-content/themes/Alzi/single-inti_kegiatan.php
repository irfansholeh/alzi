<?php 
/* Template Name: single_chapter */ 
get_header();
?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

<section style="background:#f4f4f4; margin-top: 90px; color: #000;width:100%;">
    <div style="background-size: contain; background-repeat: no-repeat; background-position: right;width: 100%;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-12 col-md-6" >
                    <h3 style="padding-top: 30px; padding-bottom: 20px; ">Kegiatan</h3>
                </div>
                <div class="col-lg-6 d-none d-sm-block" style="padding-top: 10px;">
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container" style=" padding-bottom: 120px; padding-top:50px;" >
    <div class="row">
        <?php while(have_posts()) : the_post(); ?>
		<?php
		//echo "<pre>";
		$kategori = get_the_terms( $post->ID , 'kategori_event' );
		$kategoriArray=array();
		foreach($kategori as $row) {
			$kategoriArray[] = $row->slug;
		}
		//var_dump($kategoriArray);
		?>
        <div class="col-12 col-md-8 col-xl-8 col-sm-12 "><h1 class="entry-title"><?php the_title();?></h1>
                    <p class="post-meta text-muted" style="padding-top:15px;font-size:14px"> <?php the_time('d/m/Y'); ?></span> |<a href="#" rel="category tag" style="color:#9756c7;"> Edukasi & Pelatihan </span></a></p><article id="post-85" class="post-85 page type-page status-publish hentry">
                    <div class="entry-content">
                    <?php
                        if (has_post_thumbnail()) {
                        the_post_thumbnail('post-thumbnail', ['class' => 'fto', 'title' => 'Feature image']);
                     }else{}?>
                    <br><blockquote><p style="color:#000">
                    <?php the_content();?> 
                    <?php endwhile;?>
                    </p></blockquote>
                </div> <!-- .entry-content -->          
                </article> <!-- .et_pb_post -->
<br/><br/>

</div>
                            
<div class="col-lg-4 col-12 col-md-4 col-xl-4" style="padding-left: 50px;">
    <?php get_template_part( 'sidebar' ); ?>
</div>
<style>
    
    .card {
    box-shadow: 0px 1px 2px 0px #e4e6e8;
    -webkit-box-shadow: 0px 1px 4px 0px #e4e6e8;
    -moz-box-shadow: 0px 1px 4px 0px #e4e6e8;
    margin:20px;
}
</style>
</div>
<br/><br/>

	<Br>

</div>


        </div>
<section style="background-color: #f4f4f4;">
    <div class="container"><br>
    <h3>Terkait</h3>
    <hr>
    <div class="row" style=" display: flex; flex-wrap: wrap;">
        <?php

        /*$loop = get_posts(array(
                'showposts' => -1,
                'post_type' => '',
                'taxonomy_query' => array(
                    array(
                        'taxonomy' => '',
                        'field' => 'slug',
                        'terms' =>
                    )
                )
            )
        );*/

        $args = array(
            'post_type' => 'kegiatan',
            'tax_query' => array(
                array(
                    'taxonomy' => 'kategori_event',
                    'field'    => 'slug',
                    'terms'    => $kategoriArray,
                ),
            ),
        );
        $loop = new WP_Query( $args );
        //$args = array( 'post_type' => 'kegiatan','order' => 'asc','orderby'=>'title','category_name'=> $args,);
        //$loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post();
            $setting = pods('kegiatan',get_the_id());

            ?>
            <div class=" col-sm-12 col-xs-12 col-md-4 col-12 col-xl-4"  style="display: flex;flex-direction: column;border-color:#6f3a96;">
                <div class="card" style="margin-left:2px;flex: 1;">

                    <?php
                    $featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');
                    ?>
                    <img src="<?=$featured_img_url?>" class="card-img-top"/>
                    <div class="card-body">
                        <h5 class="card-title"><?php the_title();?></h5>
                        <?php the_excerpt(); ?>
                        <a href="#" class="btn btn-primary" style="background-color:#a23db7;color:#fff;">Read More</a>
                    </div>
                </div>
            </div>
        <?php
        endwhile;
        ?>
        <div class="col-7 col-md-12 col-xl-12 align-self-end">
            <div class="row justify-content-end">
                <div class="col-2">
                    <button type="button" class="btn btn-md" style="background-color:#a23db7;color:#fff;">More Detail>></button>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<br><br>
</div>


    </div>

</div>

</div>

<?php  
get_footer();
?>
