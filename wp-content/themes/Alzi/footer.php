<footer class="ftco-footer ftco-bg-dark ftco-section" style="padding-top: 100px">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-4">
                <div class="ftco-footer-widget mb-4">
                    <h4 class="ftco-heading-2">Alzheimer Indonesia</h4>
                    <p>Alzheimer Indonesia is a non profit organization that aims to improve the quality life of people with Dementia / Alzheimer, their families and caregivers in Indonesia.</p>
                    <a href="#" class="fa fa-facebook ka"></a>
                    <a href="#" class="fa fa-youtube-play ka"></a>
                    <a href="#" class="fa fa-instagram ka"></a>
                    <a href="#" class="fa fa-twitter ka"></a>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4 ml-md-5">
                    <h4 class="ftco-heading-2">Alzheimer dan Demensia</h4>
                    <ul class="list-unstyled">
                        <li><a href="#" class="py-2 d-block ft">Alzheimer</a></li>
                        <li><a href="#" class="py-2 d-block ft">Demensia</a></li>
                        <li><a href="#" class="py-2 d-block ft">Orang Dengan Demensia (ODD)</a></li>
                        <li><a href="#" class="py-2 d-block ft">10 Gejala Alzheimer</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h4 class="ftco-heading-2">Kegiatan</h4>
                    <ul class="list-unstyled">
                        <li><a href="#" class="py-2 d-block ft">Pertemuan Caregiver</a></li>
                        <li><a href="#" class="py-2 d-block ft">World Alzheimer’s Month</a></li>
                        <li><a href="#" class="py-2 d-block ft">Remember Me Charity Concert</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h4 class="ftco-heading-2">Memerlukan Bantuan?</h4>
                    <div class="block-23 mb-3">
                        <ul style="padding-left: 20px;">
                            <li><span class="text">Jl. Jend. Sudirman No.51, RT.5/RW.4, Karet Semanggi, Kecamatan Setiabudi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12930</span></li>
                            <li><a href="#"><span class="text ft">+62 816 86 3136</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row" style="margin-top:10px;">
            <div class="col-md-12 text-center p-2" style="background-color:#290142;">

                <p style="padding-top:15px;padding-bottom:5px"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright ©<script>document.write(new Date().getFullYear());</script> Yayasan Alzheimer Indonesia
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            </div>
        </div>
    </div>
</footer>
<style type="text/css">
    .ft:hover{
        color: #fff;
    }
    .ka{
        width: 60px
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
    (function () {
        var options = {
            whatsapp: "+628112042009", // WhatsApp number
            call_to_action: "Message us", // Call to action
            position: "right", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
</body>
</html>