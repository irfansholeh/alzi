<div class="hero-wrap " style="background-image: url('https://gontornews.com/wp-content/uploads/2018/02/alzheimer.jpg');height:400px;">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-halfheight align-items-center justify-content-start" data-scrollax-parent="true">
          <div class="col-md-9 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
            <h3 class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }" style="margin-top:100px;color:#fff;">Key Message</h3>
            <p data-scrollax="properties: { translateY: '30%', opacity: 1.6 }" style="text-shadow:2px 2px #000;"><strong>Memberikan Awareness dan mengarahkan visitor sesuai dengan kebutuhan.</strong></p>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section services-section bg-light">
      <div class="container">
        <div class="row d-flex justify-content-center">
          <div class="col-md-4 d-flex align-self-stretch ftco-animate" >
            <div class="media block-6 services d-block kotaku">
              <div class="media-body p-2 mt-2">
                <h3 class="heading mb-3">Alzheimer & Demensia</h3>
                <p >Beda Alzheimer dengan Demensia</p>
                <p >Mengenali orang dengan demensia</p>
                <a href="#"><p class="huruf">Ketahui lebih lanjut>></p></a>
              </div>
            </div>      
          </div>
          <div class="col-md-4 d-flex align-self-stretch  ftco-animate">
            <div class="media block-6 services d-block kotaku">
              <div class="media-body p-2 mt-2">
                <h3 class="heading mb-3">Saya orang dengan demensia(ODD)</h3>
                <p >Recommended Link 1</p>
                <p >Recommended Link 2</p>
                <a href="#"><p class="huruf">Ketahui lebih lanjut>></p></a>
              </div>
            </div>      
          </div>
          <div class="col-md-4 d-flex align-self-stretch ftco-animate" style="">
            <div class="media block-6 services d-block kotaku">
              <div class="media-body p-2 mt-2">
                <h3 class="heading mb-3">Saya mendampingi orang(ODD)</h3>
                <p >Recommended Link 1</p>
                <p >Recommended Link 2</p>
                <a href="#"><p class="huruf">Ketahui lebih lanjut>></p></a>
              </div>
            </div>      
          </div>
        </div>
      </div>
    </section>
    
    <section class="ftco-section ftco-destination">
    	<div class="container">
    		  <div class="row" style="background-color:#fff;">
            <div class="col-12 col-md-8" style="">
              <div class="card" style="height:250px;border-color:#6f3a96;">
                <div class="card-body">
                  <h5 class="card-title">Mengenal ALZI</h5>
                  <p class="card-text">Short description of key mission ALZI.</p>
                </div>
              </div>
            </div>
            <div class="col-6 col-md-4" style="">
              <div class="card" style="height:250px;margin:2px;border-color:#6f3a96;">
                <div class="card-body">
                  <h5 class="card-title">Chapter di daerah</h5>
                  <p class="card-text">Dropdown/search based on user input&links ke daftar chapter ALZI</p>
                </div>
              </div>
            </div>
          </div>
    	</div>
    </section>

    <section class="ftco-section services-section bg-light">
      <div class="container">
        <h1>News Event</h1>
        <div class="row">
          <div class="col-12 col-sm-4">
            <div class="card" style="width: 18rem;border-color:#6f3a96;">
              <img src="https://expresstext.net/blog/wp-content/uploads/2018/11/EventMarketing.jpeg" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Event 1</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-primary huruf" style="border-radius:0;background-color:#6f3a96;border-color:#6f3a96">Lebih Lanjut</a>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-4">
            <div class="card" style="width: 18rem;border-color:#6f3a96;">
              <img src="https://expresstext.net/blog/wp-content/uploads/2018/11/EventMarketing.jpeg" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Event 2</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-primary huruf" style="border-radius:0;background-color:#6f3a96;border-color:#6f3a96">Lebih Lanjut</a>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-4">
            <div class="card" style="width: 18rem;border-color:#6f3a96;">
              <img src="https://expresstext.net/blog/wp-content/uploads/2018/11/EventMarketing.jpeg" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Event 3</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="#" class="btn btn-primary huruf" style="border-radius:0;background-color:#6f3a96;border-color:#6f3a96">Lebih Lanjut</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section ">
    	<div class="container">
				<div class="row justify-content-start mb-5 pb-3">
          <div class="col-12 col-md-12">
              <div class="card" style="height:150px;margin:2px">
                <div class="card-body">
                  <h5 class="card-title text-center">Berpartisipasi Mengatasi Alzeimer & Demensia</h5>
                  <p class="card-text">  
                    <div class="row justify-content-center">
                      <div class="col-6 col-sm-4"><button type="button" class="btn btn-outline-primary kotak1" style="width:250px">Primary</button></div>
                      <div class="col-6 col-sm-4"><button type="button" class="btn btn-outline-success kotak2" style="width:250px">Success</button></div>
                      <div class="col-6 col-sm-4"><button type="button" class="btn btn-outline-danger kotak3" style="width:250px">Danger</button></div>
                  </div>
                </p>
                </div>
              </div>
          </div>
        </div>    		
    	</div>
    </section>

    