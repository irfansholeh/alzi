<?php
/* Template Name: Saya Adalah */
get_header();
?>
<section style="background:#9254bf; margin-top: 90px; color: #ffffff;">
    <?php
    /*$loop = get_posts(array(
            'showposts' => -1,
            'post_type' => '',
            'taxonomy_query' => array(
                array(
                    'taxonomy' => '',
                    'field' => 'slug',
                    'terms' =>
                )
            )
        )
    );*/

    $args = array(
        'post_type' => 'post',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field'    => 'slug',
                'terms'    => array($_GET['kategori']),
            ),
        ),
    );
    $loop = new WP_Query( $args );
    //$args = array( 'post_type' => 'kegiatan','order' => 'asc','orderby'=>'title','category_name'=> $args,);
    //$loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post();
    $setting = pods('kegiatan',get_the_id());

    ?>
    <div style="background-size: contain; background-repeat: no-repeat; background-position: right;background-color:#f4f4f4;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6" >
                    <h3 style="padding-top: 30px; padding-bottom: 20px;color:#000"><?php $explode = explode('=',($_SERVER['REQUEST_URI'])); echo $explode[1];?></h3>
                </div>
                <div class="col-lg-6 d-none d-sm-block" style="padding-top: 15px;">
                </div>
            </div>
        </div>
    </div>
    <?php
    endwhile;
    ?>
</section>
<div class="container" style=" padding-bottom: 120px; padding-top:50px;" >
    <div class="row" style=" display: flex; flex-wrap: wrap;">
        <?php
        /*$loop = get_posts(array(
                'showposts' => -1,
                'post_type' => '',
                'taxonomy_query' => array(
                    array(
                        'taxonomy' => '',
                        'field' => 'slug',
                        'terms' =>
                    )
                )
            )
        );*/

        $args = array(
            'post_type' => 'post',
            'tax_query' => array(
                array(
                    'taxonomy' => 'category',
                    'field'    => 'slug',
                    'terms'    => array($_GET['kategori']),
                ),
            ),
        );
        $loop = new WP_Query( $args );
        //$args = array( 'post_type' => 'kegiatan','order' => 'asc','orderby'=>'title','category_name'=> $args,);
        //$loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post();
            $setting = pods('kegiatan',get_the_id());

            ?>
            <div class=" col-sm-12 col-xs-12 col-md-4 col-12 "  style="display: flex;flex-direction: column;border-color:#6f3a96;">
                <div class="card" style="margin-left:2px;flex: 1;">

                    <?php
                    $featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');
                    ?>
                    <img src="<?=$featured_img_url?>" class="card-img-top"/>
                    <div class="card-body">
                        <h5 class="card-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h5>
                        <p>
                        <?php $content = get_the_content();
                        $trimmed_content = wp_trim_words($content,'50',true); ?>
                        <?php echo $trimmed_content; ?>
                        </p>
                        <button type="button" class="btn btn-md pull-right" style="background-color:#a23db7;color:#fff;">More Detail</button>
                    </div>
                </div>
            </div>
        <?php
        endwhile;
        ?>
    </div>
    <Br>
</div>
</div>
</div>
</div>
</div>
<?php
get_footer();
?>
