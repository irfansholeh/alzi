<?php /* Template Name: Donate */ ?>
<?php
get_header();
$countries = array("AF" => "Afghanistan",
"AX" => "Åland Islands",
"AL" => "Albania",
"DZ" => "Algeria",
"AS" => "American Samoa",
"AD" => "Andorra",
"AO" => "Angola",
"AI" => "Anguilla",
"AQ" => "Antarctica",
"AG" => "Antigua and Barbuda",
"AR" => "Argentina",
"AM" => "Armenia",
"AW" => "Aruba",
"AU" => "Australia",
"AT" => "Austria",
"AZ" => "Azerbaijan",
"BS" => "Bahamas",
"BH" => "Bahrain",
"BD" => "Bangladesh",
"BB" => "Barbados",
"BY" => "Belarus",
"BE" => "Belgium",
"BZ" => "Belize",
"BJ" => "Benin",
"BM" => "Bermuda",
"BT" => "Bhutan",
"BO" => "Bolivia",
"BA" => "Bosnia and Herzegovina",
"BW" => "Botswana",
"BV" => "Bouvet Island",
"BR" => "Brazil",
"IO" => "British Indian Ocean Territory",
"BN" => "Brunei Darussalam",
"BG" => "Bulgaria",
"BF" => "Burkina Faso",
"BI" => "Burundi",
"KH" => "Cambodia",
"CM" => "Cameroon",
"CA" => "Canada",
"CV" => "Cape Verde",
"KY" => "Cayman Islands",
"CF" => "Central African Republic",
"TD" => "Chad",
"CL" => "Chile",
"CN" => "China",
"CX" => "Christmas Island",
"CC" => "Cocos (Keeling) Islands",
"CO" => "Colombia",
"KM" => "Comoros",
"CG" => "Congo",
"CD" => "Congo, The Democratic Republic of The",
"CK" => "Cook Islands",
"CR" => "Costa Rica",
"CI" => "Cote D'ivoire",
"HR" => "Croatia",
"CU" => "Cuba",
"CY" => "Cyprus",
"CZ" => "Czech Republic",
"DK" => "Denmark",
"DJ" => "Djibouti",
"DM" => "Dominica",
"DO" => "Dominican Republic",
"EC" => "Ecuador",
"EG" => "Egypt",
"SV" => "El Salvador",
"GQ" => "Equatorial Guinea",
"ER" => "Eritrea",
"EE" => "Estonia",
"ET" => "Ethiopia",
"FK" => "Falkland Islands (Malvinas)",
"FO" => "Faroe Islands",
"FJ" => "Fiji",
"FI" => "Finland",
"FR" => "France",
"GF" => "French Guiana",
"PF" => "French Polynesia",
"TF" => "French Southern Territories",
"GA" => "Gabon",
"GM" => "Gambia",
"GE" => "Georgia",
"DE" => "Germany",
"GH" => "Ghana",
"GI" => "Gibraltar",
"GR" => "Greece",
"GL" => "Greenland",
"GD" => "Grenada",
"GP" => "Guadeloupe",
"GU" => "Guam",
"GT" => "Guatemala",
"GG" => "Guernsey",
"GN" => "Guinea",
"GW" => "Guinea-bissau",
"GY" => "Guyana",
"HT" => "Haiti",
"HM" => "Heard Island and Mcdonald Islands",
"VA" => "Holy See (Vatican City State)",
"HN" => "Honduras",
"HK" => "Hong Kong",
"HU" => "Hungary",
"IS" => "Iceland",
"IN" => "India",
"ID" => "Indonesia",
"IR" => "Iran, Islamic Republic of",
"IQ" => "Iraq",
"IE" => "Ireland",
"IM" => "Isle of Man",
"IL" => "Israel",
"IT" => "Italy",
"JM" => "Jamaica",
"JP" => "Japan",
"JE" => "Jersey",
"JO" => "Jordan",
"KZ" => "Kazakhstan",
"KE" => "Kenya",
"KI" => "Kiribati",
"KP" => "Korea, Democratic People's Republic of",
"KR" => "Korea, Republic of",
"KW" => "Kuwait",
"KG" => "Kyrgyzstan",
"LA" => "Lao People's Democratic Republic",
"LV" => "Latvia",
"LB" => "Lebanon",
"LS" => "Lesotho",
"LR" => "Liberia",
"LY" => "Libyan Arab Jamahiriya",
"LI" => "Liechtenstein",
"LT" => "Lithuania",
"LU" => "Luxembourg",
"MO" => "Macao",
"MK" => "Macedonia, The Former Yugoslav Republic of",
"MG" => "Madagascar",
"MW" => "Malawi",
"MY" => "Malaysia",
"MV" => "Maldives",
"ML" => "Mali",
"MT" => "Malta",
"MH" => "Marshall Islands",
"MQ" => "Martinique",
"MR" => "Mauritania",
"MU" => "Mauritius",
"YT" => "Mayotte",
"MX" => "Mexico",
"FM" => "Micronesia, Federated States of",
"MD" => "Moldova, Republic of",
"MC" => "Monaco",
"MN" => "Mongolia",
"ME" => "Montenegro",
"MS" => "Montserrat",
"MA" => "Morocco",
"MZ" => "Mozambique",
"MM" => "Myanmar",
"NA" => "Namibia",
"NR" => "Nauru",
"NP" => "Nepal",
"NL" => "Netherlands",
"AN" => "Netherlands Antilles",
"NC" => "New Caledonia",
"NZ" => "New Zealand",
"NI" => "Nicaragua",
"NE" => "Niger",
"NG" => "Nigeria",
"NU" => "Niue",
"NF" => "Norfolk Island",
"MP" => "Northern Mariana Islands",
"NO" => "Norway",
"OM" => "Oman",
"PK" => "Pakistan",
"PW" => "Palau",
"PS" => "Palestinian Territory, Occupied",
"PA" => "Panama",
"PG" => "Papua New Guinea",
"PY" => "Paraguay",
"PE" => "Peru",
"PH" => "Philippines",
"PN" => "Pitcairn",
"PL" => "Poland",
"PT" => "Portugal",
"PR" => "Puerto Rico",
"QA" => "Qatar",
"RE" => "Reunion",
"RO" => "Romania",
"RU" => "Russian Federation",
"RW" => "Rwanda",
"SH" => "Saint Helena",
"KN" => "Saint Kitts and Nevis",
"LC" => "Saint Lucia",
"PM" => "Saint Pierre and Miquelon",
"VC" => "Saint Vincent and The Grenadines",
"WS" => "Samoa",
"SM" => "San Marino",
"ST" => "Sao Tome and Principe",
"SA" => "Saudi Arabia",
"SN" => "Senegal",
"RS" => "Serbia",
"SC" => "Seychelles",
"SL" => "Sierra Leone",
"SG" => "Singapore",
"SK" => "Slovakia",
"SI" => "Slovenia",
"SB" => "Solomon Islands",
"SO" => "Somalia",
"ZA" => "South Africa",
"GS" => "South Georgia and The South Sandwich Islands",
"ES" => "Spain",
"LK" => "Sri Lanka",
"SD" => "Sudan",
"SR" => "Suriname",
"SJ" => "Svalbard and Jan Mayen",
"SZ" => "Swaziland",
"SE" => "Sweden",
"CH" => "Switzerland",
"SY" => "Syrian Arab Republic",
"TW" => "Taiwan, Province of China",
"TJ" => "Tajikistan",
"TZ" => "Tanzania, United Republic of",
"TH" => "Thailand",
"TL" => "Timor-leste",
"TG" => "Togo",
"TK" => "Tokelau",
"TO" => "Tonga",
"TT" => "Trinidad and Tobago",
"TN" => "Tunisia",
"TR" => "Turkey",
"TM" => "Turkmenistan",
"TC" => "Turks and Caicos Islands",
"TV" => "Tuvalu",
"UG" => "Uganda",
"UA" => "Ukraine",
"AE" => "United Arab Emirates",
"GB" => "United Kingdom",
"US" => "United States",
"UM" => "United States Minor Outlying Islands",
"UY" => "Uruguay",
"UZ" => "Uzbekistan",
"VU" => "Vanuatu",
"VE" => "Venezuela",
"VN" => "Viet Nam",
"VG" => "Virgin Islands, British",
"VI" => "Virgin Islands, U.S.",
"WF" => "Wallis and Futuna",
"EH" => "Western Sahara",
"YE" => "Yemen",
"ZM" => "Zambia",
"ZW" => "Zimbabwe");
?>
  <section style="background:#f4f4f4; margin-top: 90px; color: #000">
		<div style="background-size: contain; background-repeat: no-repeat; background-position: right">
			<div class="container">
				<div class="row">
					<div class="col-lg-6" >
						<h3 style="padding-top: 30px; padding-bottom: 20px; ">Donasi</h3>
					</div>
					<div class="col-lg-6 d-none d-sm-block" style="padding-top: 15px;">
					</div>
				</div>
			</div>
		</div>
	</section><br>
	<section>
		<div class="container">
		<form>
			<h2 style="color:#6f3a96;">Isi Data anda</h2>
			<hr>
		  <div class="form-row">
			<div class="form-group col-md-8 col-12">
			  <label for="inputEmail4">Nama Lengkap</label>
			  <input type="text" class="form-control" id="inputEmail4" placeholder="Nama Lengkap">
			</div>
		  </div>
		  <div class="form-row">
			<div class="form-group col-md-8 col-12">
			  <label for="inputEmail4">Email</label>
			  <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
			</div>
		  </div>
           <div class="form-row">
                <div class="form-group col-md-8 col-12">
                    <label for="inputEmail4">Nomor Handphone</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="Nomor Handphone">
                </div>
            </div>
		  <div class="form-row">
		  <div class="form-group col-md-8 col-12">
			<label for="inputAddress">Alamat</label>
			<input type="text" class="form-control" id="inputAddress" placeholder="Alamat">
		  </div>
		  </div>
		  <div class="form-row">
			<div class="form-group col-md-3 col-5">
			  <label for="inputCity">Kota</label>
			  <input type="text" class="form-control" id="inputCity" placeholder="kota">
			</div>
			<div class="form-group col-md-3 col-4">
			  <label for="inputState">Negara</label>
			  <select id="inputState" class="form-control">
			  <?php foreach($countries as $key => $value) { ?>
				<option value="<?= $key ?>" title="<?= htmlspecialchars($value) ?>"><?= htmlspecialchars($value) ?></option>
				<?php } ?>
			  </select>
			</div>
			<div class="form-group col-md-2 col-3">
			  <label for="inputZip">Kode Pos</label>
			  <input type="text" class="form-control" id="inputZip" placeholder="Kode Pos">
			</div>
		  </div>
		  <div class="form-row">
		    <div class="form-group col-12 col-md-8">
			<label for="inputZip">Hubungan Anda</label>
				<select class="custom-select">
					<option value="Saya menderita Alzheimer atau demensia.">Saya menderita Alzheimer atau demensia.</option>
					<option value="Saya mendukung atau merawat seseorang dengan Alzheimer atau demensia.">Saya mendukung atau merawat seseorang dengan Alzheimer atau demensia.</option>
					<option value="Saya telah kehilangan seseorang karena Alzheimer atau demensia.">Saya telah kehilangan seseorang karena Alzheimer atau demensia.</option>
					<option value="Saya tidak memiliki hubungan dekat, tetapi mendukung visi dunia tanpa Alzheimer.">Saya tidak memiliki hubungan dekat, tetapi mendukung visi dunia tanpa Alzheimer.</option>
					<option value="Saya lebih suka tidak menjawab.">Saya lebih suka tidak menjawab.</option>
				</select>
			</div>
		   </div>
		   <div class="form-row">
			<div class="form-group col-md-8 col-12">
			  <label for="inputEmail4">Jumlah Donasi</label>
			  <input type="text" class="form-control" id="inputEmail4" placeholder="Rp.100.000.000,00-">
			</div>
		  </div>
		  <div class="form-row">
			<div class="form-group col-md-8 col-12">
			  <label for="inputEmail4">Atas Nama</label>
			  <input type="text" class="form-control" id="inputEmail4" placeholder="Atas Nama">
			</div>
		  </div>
		  <div class="form-row">
			<div class="form-group col-md-8 col-12">
			  <label for="inputEmail4">Nomor Kartu ATM</label>
			  <input type="text" class="form-control" id="inputEmail4" placeholder="Nomor Kartu ATM">
			</div>
		  </div>
		  <div class="form-row">
			<div class="form-group col-md-3 col-4">
			  <label for="inputCity">Kadaluarsa</label>
			  <input type="text" class="form-control" id="inputCity" placeholder="16/30">
			</div>
			<div class="form-group col-md-3 col-4">
			  <label for="inputCity">CCV</label>
			  <input type="text" class="form-control" id="inputCity" placeholder="444">
			</div>
		  </div>
		  <div class="form-row">
			<div class="form-group col-md-3 col-4">
			  <button type="button" class="btn" style="background-color:#6f3a96;color:#fff;width:100%;height:100%;">Kirim</button>
			</div>
		  </div>
		   
		</form>
		</div>
	</section>
	<br><br>
  

<?php
get_footer();
?>