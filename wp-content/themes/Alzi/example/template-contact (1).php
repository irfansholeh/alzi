<?php
/**
 * Template Name: Contact
 */

$id = get_the_ID();
$pods = pods('page',$id);
$title = get_the_title();
$subtitle = $pods->field('subtitle_bottom');
$headerImage = $pods->field('header_image');
$headerImage = wp_get_attachment_url($headerImage['ID']);

$mapUrl = get_option('contact_map_url');
$buttonLabel = get_option('contact_button_label');

get_header(); ?>

<?php
$insert  = array(
    'post_type' => 'inbox',
    'post_title'=>$_GET['subject'],
    'post_content' => $_GET['message']
);
$PostID = wp_insert_post($insert);
add_post_meta($PostID,'topic',$_GET['topic']);
add_post_meta($PostID,'email',$_GET['email']);
?>
<section style=" margin-top:60px; background: #020037 url(<?=$headerImage?>) right no-repeat; height: 250px">


    <div class="container text-white">
        <h1 class="pt-100px"><?=get_the_title()?><br/><small style="font-size: 18px"><?=$subtitle?></small></h1>
    </div>
</section>

<section style="padding-top: 70px; padding-bottom: 70px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">

                <form action="" method="get">
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Topic</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="topic">
                                <option value="general">General</option>
                                <option value="event marketing">Event Marketing</option>
                                <option value="creative service">Creative Service</option>
                                <option value="intelectual property">Intellectual Property</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email" name="email" class="form-control" required placeholder="Your Email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Subject</label>
                        <div class="col-sm-10">
                            <input type="text" name="subject" class="form-control" placeholder="Subject of your message">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Message</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="message" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-10"><br/>
                            <input type="submit" class="btn btn-primary" style="background: #010025; border-color: #010025; border-radius: 20px; padding-left: 30px; padding-right: 30px;" value="<?=$buttonLabel?>"/>
                        </div>
                    </div>
                </form>

                <br/>
            </div>
            <div class="col-lg-5">
                <iframe src="<?=$mapUrl?>" width="100%" height="380" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>