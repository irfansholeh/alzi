

<!--HERO-->
<section id="hero" style="">
    <div class="container">
        <div class="d-lg-none">
            <?php
            $args = array(
                'page_id' => '82',
            );
            $wp_query = new WP_Query( $args );
            if($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();
            ?>
            <img src="<?= get_the_post_thumbnail_url(); ?>" style="width: 100%; height: auto;">
            <?php endwhile; endif; ?>
        </div>
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="row" style="padding-top: 15%;">
                    <div class="col-12">
                        <h1 class="judul1">Inovasi Untuk Indonesia</h1>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-9">
                        <p style="color: black;">Sawala adalah pengembangan software berbasis cloud untuk bisnis</p>
                    </div>

                </div>
                <br>
                <div class="row">
                    <div class="col-5">
                        <a href="#layanan"><button class="btn btn-sawala" style="border-radius: 20px;">Layanan Kami</button></a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 d-none d-lg-block">
                <?php
                $args = array(
                    'page_id' => '82',
                );
                $wp_query = new WP_Query( $args );
                if($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();
                ?>
                <img src="<?= get_the_post_thumbnail_url(); ?>" style="width: 100%; height: auto;">
                <?php endwhile; endif; ?>
            </div>
        </div>
    </div>
</section>


<!--LAYANAN-->

<section style="" class="" id="layanan">
    <div class="container" style="">
        <div class="row">
            <?php
            $args = array(
                'page_id' => '144',
            );
            $wp_query = new WP_Query( $args );
            if($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();
                ?>
                <?php the_content(); ?>
            <?php endwhile; endif; ?>
            <div class="col-12 col-lg-7 pt-mobile">
                <div class="row">

                        <?php
                        $args = array( 'post_type' => 'layanan','order' => 'Desc','orderby'=>'title','posts_per_page'=>-1);
                        $loop = new WP_Query( $args );
                        while ( $loop->have_posts() ) : $loop->the_post();

                            $setting = pods('layanan',get_the_id());
                            $bio = $setting->field('bio');

                            ?>
                        <div class="col-6" style="margin-bottom: 15px;">
                        <img class="img-product" src="<?= get_the_post_thumbnail_url(); ?>">
                        <p class="title-product"><?php the_title();?></p>
                        <p class="content-product"><?php the_content();?></p>
                        </div>
                    <?php
                    endwhile;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="body-testimony" id="client">
    <!--CLIENT-->
    <div class="container">
        <?php
        $args = array(
            'page_id' => '141',
        );
        $wp_query = new WP_Query( $args );
        if($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();
            ?>
            <?php the_content(); ?>
        <?php endwhile; endif; ?>

        <div class="client" style="margin: 15px;">
            <?php
            $args = array( 'post_type' => 'company','order' => 'ASC','orderby'=>'title','posts_per_page'=>-1);
            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post();

            $setting = pods('company',get_the_id());
            $bio = $setting->field('bio');

            ?>
            <div>
                <div class="card card-company">
                    <img class="company" src="<?= get_the_post_thumbnail_url(); ?>">
                </div>
            </div>
                <?php
            endwhile;
            ?>
        </div>
    </div>
</section>
<section>
    <div class="container-fluid card-comment" style="position: relative;margin: 70px 0px 0px 0px; background-color: white;">
        <div class="card-comment-background-2">
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/Testimoni_Art (800x).png" style="width: 45%; float: right; height: auto;">
        </div>
        <div class="container">
            <div class="testi">
                <?php
                $args = array( 'post_type' => 'client','order' => 'ASC','orderby'=>'title','posts_per_page'=>-1);
                $loop = new WP_Query( $args );
                while ( $loop->have_posts() ) : $loop->the_post();

                $setting = pods('client',get_the_id());
                $jbtn = $setting->field('jabatan');

                ?>
                <div>
                    <div class="row">
                        <div class="col-lg-7 col-12" style="align-self: center;">
                            <p><?php the_content();?></p>
                            <div class="garis-3"></div>
                        </div>
                        <div class="col-lg-2 col-6" style="border-radius: 100%; align-self: center; padding: 0;">
                            <img src="<?= get_the_post_thumbnail_url(); ?>" width="50%" height="auto" style="border-radius: 100%;margin-left: 30px; margin-top: 10px;">
                        </div>
                        <div class="col-lg-3 col-6" style="align-self: center;">
                            <p class="title-product" style=" margin-bottom: 0px;"><b><?php the_title()?></b></p>
                            <p><?=$jbtn?></p>
                        </div>
                    </div>
                </div>
                    <?php
                endwhile;
                ?>
            </div>
        </div>
    </div>

</section>

<section style="background-color: #6293C8; padding: 130px 0px;">

    <!--    PRODUK-->
    <div class="container" style="" id="produk">
        <div class="container card-penawaran">
            <div class="row" style="">
                <div class="col-12">
                    <?php
                    $args = array(
                        'page_id' => '139',
                    );
                    $wp_query = new WP_Query( $args );
                    if($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();
                        ?>
                        <?php the_content(); ?>
                    <?php endwhile; endif; ?>
                    <div class="row">
                        <?php
                        $args = array( 'post_type' => 'produk','order' => 'ASC','orderby'=>'title','posts_per_page'=>-1);
                        $loop = new WP_Query( $args );
                        while ( $loop->have_posts() ) : $loop->the_post();

                        $setting = pods('produk',get_the_id());
                        //$jbtn = $setting->field('jabatan');

                        ?>
                        <div class="col-lg-4 col-6 background-product" style="margin-bottom: 20px;">
                            <img class="img-product" src="<?= get_the_post_thumbnail_url(); ?>">
                            <p class="title-product"><?php the_title(); ?></p>
                            <p class="content-product"><?php the_content(); ?></p>
                        </div>
                            <?php
                        endwhile;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid" style="padding: 10% 5%;" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-12 text-center">

                <?php
                $args = array(
                    'page_id' => '158',
                );
                $wp_query = new WP_Query( $args );
                if($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();
                    ?>
                    <img src="<?= get_the_post_thumbnail_url(); ?>" style="width: 80%; height: auto;">
                <?php endwhile; endif; ?>
            </div>
            <div class="col-lg-5 col-12">
                <div class="row">
                    <h4 class="judul2">Mari berkolaborasi</h4>
                    <p style="font-size: 15px;">Tunggu apalagi. Hubungi kami untuk diskusi lebih lanjut.</p>
                </div>
                <form method="post" action="http://localhost/sawala/?page_id=174&preview=true" id="contactForm">
                <div class="row" id="formContact" style="">
                    <input class="form-control placeholder-blue" type="text" name="name" placeholder="Nama Lengkap" required>
                    <input class="form-control placeholder-blue" type="text" name="company" placeholder="Perusahaan" required>
                    <input class="form-control placeholder-blue" type="text" name="email" placeholder="Email" required>
                    <input class="form-control placeholder-blue" type="text" name="phone" placeholder="No. Hp" required>
                    <textarea class="form-control placeholder-blue"name="pesan" placeholder="Pesan" required></textarea>
                </div>
                <div class="row">
                    <div id="statusSubmit" class="col-12" style="padding: 0;"></div>
                    <input type="submit" class="btn btn-sawala-2" style="width: 30%;" value="Kirim">
                </div>
                </form>
            </div>
        </div>
    </div>
</div>


