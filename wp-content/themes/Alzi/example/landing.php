<!DOCTYPE html>
<html>
<head>
	<title>Sawala Website</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/bootstrap/js/jquery-3.2.1.slim.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/bootstrap/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/bootstrap/js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/bootstrap/js/popper.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/bootstrap/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/landing.css">
</head>
<section style="margin-top: 5%;"> 
<div class="container"> 
	<header>
	  <nav class="navbar navbar-expand-lg">

	    <div class="navbar-brand mr-auto"> 
	    		<a href="#"><img class="logo" src="<?php bloginfo('template_directory'); ?>/assets/img/Sawala_Logo (600x).png"></a>
		</div>

	    <button class="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
	            <span class="navbar-toggler-icon"></span>
	    </button>


	    <div class="navbar-collapse collapse" id="navbarSupportedContent">
	      <ul class="navbar-nav ml-auto">
	          <li class="nav-item">
	            <a class="nav-link" href="#">Beranda</a>
	          </li>
	          <li class="nav-item">
	            <a class="nav-link" href="#">Layanan</a>
	          </li>
	          <li class="nav-item">
	            <a class="nav-link" href="#">Produk</a>
	          </li>
	          <li class="nav-item">
	            <a class="nav-link" href="#">Perusahaan</a>
	          </li>
	          <li class="nav-item">
	            <a href="#"><button class="btn btn-sawala">Hubungi Kami</button></a>
	          </li>
	      </ul>
	    </div>
	  </nav>
	</header>
</div>
</section>
<body style="background-image: url(./assets/img/Background_Art.png); background-repeat: no-repeat;     background-position-x: right;
    background-position-y: top;">
	<section>
		<div class="container">
			<div class="row">
				<div class="col-5">
					<div class="row" style="padding-top: 20%;">
						<div class="col-12">
							<h1 class="judul1">Inovasi Untuk Indonesia</h1>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-9">
							<p>Sawala adalah pengembangan software berbasis cloud untuk bisnis</p>
						</div>
						
					</div>
					<br>
					<div class="row">
						<div class="col-5">
							<a href="#"><button class="btn btn-sawala" style="border-radius: 20px;">Lihat Studi Kasus</button></a>
						</div>
					</div>
				</div>
				<div class="col-7">
					<img src="./assets/img/Home_Art (800x).png" style="width: 100%; height: auto;">
				</div>
			</div>
		</div>
	</section>
	
	<section style="margin-top: 20%;">
		<div class="container" style="padding: 0px 50px;">
			<div class="row">
				<div class="col-6" style="align-self: flex-end;">
					<h4 class="judul2">Berpengalaman membangun solusi untuk bisnis</h4>
				</div>
				<div class="col-3">
					<img class="img-product" src="./assets/img/Icon_Website (200x).png">
					<p class="title-product">Website</p>
					<p class="content-product">Bangun rumah digital dengan website tinggi berbasis wordpress</p>
				</div>
				<div class="col-3">
					<img class="img-product" src="./assets/img/Icon_Sistem_Info (200x).png">
					<p class="title-product">Sistem Informasi</p>
					<p class="content-product">Otomatisasi proses bisnis dengan sistem terintegritasi</p>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="garis-1"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-6">
					<p style="font-family: 'Montserrat';">Kami siap membantu bisnis anda lebih efektif dengan memanfaatkan teknologi cloud</p>
				</div>
				<div class="col-3">
					<img class="img-product" src="./assets/img/Icon_App_Mobile (200x).png">
					<p class="title-product">Aplikasi Mobile</p>
					<p class="content-product">Hadirkan pengalaman pelanggan melalui genggaman</p>
				</div>
				<div class="col-3">
					<img class="img-product" src="assets/img/Icon_Perangkat_Cloud (200x)1.png">
					<p class="title-product">Perangkat Cloud</p>
					<p class="content-product">Kami juga ahli soal G Suite dan Zahir accounting</p>
				</div>
			</div>
		</div>
	</section>

	<section class="body-testimony">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-4">
					<h5 class="title-testimony">Dipercaya Perusahaan Ternama</h5>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-1">
					<div class="garis-2"></div>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-4">
					<p style="font-size: 12px; text-align: center;">Anda bersama perusahaan ternama lainya. Klien kami berasal dari berbagai industri</p>
				</div>
			</div>
			<div class="row">
				<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
		            <ol class="carousel-indicators">
		            	<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
		                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
		            </ol>
		            <div class="carousel-inner">
		                <div class="carousel-item active">
		                	<div class="row justify-content-center">
								<div class="col-2 card card-company">
									<img class="company" src="./assets/img/Tempo_Institute (600x).png">
								</div>
								<div class="col-2 card card-company">
									<img class="company" src="./assets/img/Media_Marketeers (600x).png">
								</div>
								<div class="col-2 card card-company">
									<img class="company" src="./assets/img/Popcon_Asia (600x).png">
								</div>
								<div class="col-2 card card-company">
									<img class="company" src="./assets/img/Telkomsel (600x).png">
								</div>
			                </div>
		                </div>
		                <div class="carousel-item">
		                	<div class="row justify-content-center">
								<div class="col-2 card card-company">
									<img class="company" src="./assets/img/Instellar (600x).png">
								</div>
								<div class="col-2 card card-company">
									<img class="company" src="./assets/img/Kosmik (600x).png">
								</div>
								<div class="col-2 card card-company">
									<img class="company" src="./assets/img/Pionicon (600x).png">
								</div>
								<div class="col-2 card card-company">
									<img class="company" src="./assets/img/Empassion (600x).png">
								</div>
			                </div>
						</div>
		                <div class="carousel-item">
		                	<div class="row justify-content-center">
								<div class="col-2 card card-company">
									<img class="company" src="./assets/img/Asuransi_Astra (600x).png">
								</div>
								<div class="col-2 card card-company">
									<img class="company" src="./assets/img/Torajamelo (600x).png">
								</div>
								<div class="col-2 card card-company">
									<img class="company" src="./assets/img/Telkom_Indo (600x).png">
								</div>
			                </div>
		                </div>
		              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
		                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		                <span class="sr-only">Previous</span>
		              </a>
		              <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
		                <span class="carousel-control-next-icon" aria-hidden="true"></span>
		                <span class="sr-only">Next</span>
		              </a>
		            </div>
		        </div>
			</div>
		</div>
		<br><br><br><br>
		<div class="container-fluid container-penawaran">
			<div class="container card-penawaran">
				<div class="row" style="padding: 0px 50px;">
					<div class="col-12">
						<div class="row">
							<div class="col-5">
								<h5 class="title-testimony">Solusi bisnis yang sudah teruji. Sederhana dan siap Anda gunakan</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-1">
								<div class="garis-2"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-5">
								<p style="font-size: 12px;">Ribuan transaksi, siap terintegrasi dengan berbagai metode pembayaran. Dalam hitungan menit, siap membantu Anda Mengoptimalkan bisnis.</p>
							</div>
						</div>
						<div class="row">
							<div class="col-4 background-product">
								<img class="img-product" src="./assets/img/Icon_Commerce (200x).png">
								<p class="title-product">Commerce</p>
								<p class="content-product">Toko online sederhana. Siap pembayaran kartu kredit & biaya pengiriman otomatis.</p>
							</div>
							<div class="col-4 background-product">
								<img class="img-product" src="./assets/img/Icon_Expo (200x).png">
								<p class="title-product">Expo</p>
								<p class="content-product">Otomatisasi penjualan booth pameran dan kelola berbagai proses.</p>
							</div>
							<div class="col-4 background-product">
								<img class="img-product" src="./assets/img/Icon_Ticket (200x).png">
								<p class="title-product">Ticket</p>
								<p class="content-product">Lakukan penjualan tiket dengan brand Anda. Kami yang memastikan hal teknis.</p>
							</div>
							<div class="col-4 background-product">
								<img class="img-product" src="./assets/img/Icon_Invoice (200x).png">
								<p class="title-product">Invoice</p>
								<p class="content-product">Penagihan otomatis, ringan dan sederhana.</p>
							</div>
							<div class="col-4 background-product">
								<img class="img-product" src="./assets/img/Icon_Form (200x).png">
								<p class="title-product">Form</p>
								<p class="content-product">Lebih dari sekedar form isian, ini bisa menerima pembayaran.</p>
							</div>
							<div class="col-4 background-product">
								<img class="img-product" src="assets/img/Icon_Perangkat_Cloud (200x)1.png">
								<p class="title-product">Suite</p>
								<p class="content-product">Dapatkan semuanya dalam satu paket aplikasi cloud.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section class="container-comment">
    	<div class="container-comment-background">
    		<img src="./assets/img/Testimoni_Art (1000x).png" width="60%" height="auto">
    	</div>
		<div class="container">
			<div class="row">
				<div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
		            <ol class="carousel-indicators">
		            	<li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
		                <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
		                <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
		            </ol>
		            <div class="carousel-inner">
		                <div class="carousel-item active">
		                  <div class="row justify-content-center">
							<div class="col-3 card card-comment">
								<div class="card-comment-background">
									<img src="./assets/img/Testimoni_Art (800x).png" style="width: 100%; height: auto;">	
								</div>
								<i class="fa fa-quote-left fa-2x quote-logo"></i>
								<p class="content-comment">
									Desain profesional dan Produk Berkualitas. Pelayanan yang diberikan sangat memuaskan.
								</p>
								<p class="tester">Pak Anna</p>
								<p class="company">Telkom</p>
							</div>
							<div class="col-3 card card-comment">
								<div class="card-comment-background">
									<img src="./assets/img/Testimoni_Art (800x).png" style="width: 100%; height: auto;">	
								</div>
								<i class="fa fa-quote-left fa-2x quote-logo"></i>
								<p style="margin-bottom: 50%; font-size: 14px;">
									Desain profesional dan Produk Berkualitas. Pelayanan yang diberikan sangat memuaskan.
								</p>
								<p class="tester">Pak Anna</p>
								<p class="company">Telkom</p>
							</div>
							<div class="col-3 card card-comment">
								<div class="card-comment-background">
									<img src="./assets/img/Testimoni_Art (800x).png" style="width: 100%; height: auto;">	
								</div>
								<i class="fa fa-quote-left fa-2x quote-logo"></i>
								<p style="margin-bottom: 50%; font-size: 14px;">
									Desain profesional dan Produk Berkualitas. Pelayanan yang diberikan sangat memuaskan.
								</p>
								<p class="tester">Pak Anna</p>
								<p class="company">Telkom</p>
							</div>
		                  </div>
		                </div>
		                <div class="carousel-item">
		                  <div class="row justify-content-center">
							<div class="col-3 card card-comment">
								<div class="card-comment-background">
									<img src="./assets/img/Testimoni_Art (800x).png" style="width: 100%; height: auto;">	
								</div>
								<i class="fa fa-quote-left fa-2x quote-logo"></i>
								<p style="margin-bottom: 50%; font-size: 14px;">
									Desain profesional dan Produk Berkualitas. Pelayanan yang diberikan sangat memuaskan.
								</p>
								<p class="tester">Pak Anna</p>
								<p class="company">Telkom</p>
							</div>
							<div class="col-3 card card-comment">
								<div class="card-comment-background">
									<img src="./assets/img/Testimoni_Art (800x).png" style="width: 100%; height: auto;">	
								</div>
								<i class="fa fa-quote-left fa-2x quote-logo"></i>
								<p style="margin-bottom: 50%; font-size: 14px;">
									Desain profesional dan Produk Berkualitas. Pelayanan yang diberikan sangat memuaskan.
								</p>
								<p class="tester">Pak Anna</p>
								<p class="company">Telkom</p>
							</div>
							<div class="col-3 card card-comment">
								<div class="card-comment-background">
									<img src="./assets/img/Testimoni_Art (800x).png" style="width: 100%; height: auto;">	
								</div>
								<i class="fa fa-quote-left fa-2x quote-logo"></i>
								<p style="margin-bottom: 50%; font-size: 14px;">
									Desain profesional dan Produk Berkualitas. Pelayanan yang diberikan sangat memuaskan.
								</p>
								<p class="tester">Pak Anna</p>
								<p class="company">Telkom</p>
							</div>
		                  </div>
						</div>
		                <div class="carousel-item">
		                  <div class="row justify-content-center">
							<div class="col-3 card card-comment">
								<div class="card-comment-background">
									<img src="./assets/img/Testimoni_Art (800x).png" style="width: 100%; height: auto;">	
								</div>
								<i class="fa fa-quote-left fa-2x quote-logo"></i>
								<p style="margin-bottom: 50%; font-size: 14px;">
									Desain profesional dan Produk Berkualitas. Pelayanan yang diberikan sangat memuaskan.
								</p>
								<p class="tester">Pak Anna</p>
								<p class="company">Telkom</p>
							</div>
							<div class="col-3 card card-comment">
								<div class="card-comment-background">
									<img src="./assets/img/Testimoni_Art (800x).png" style="width: 100%; height: auto;">	
								</div>
								<i class="fa fa-quote-left fa-2x quote-logo"></i>
								<p style="margin-bottom: 50%; font-size: 14px;">
									Desain profesional dan Produk Berkualitas. Pelayanan yang diberikan sangat memuaskan.
								</p>
								<p class="tester">Pak Anna</p>
								<p class="company">Telkom</p>
							</div>
							<div class="col-3 card card-comment">
								<div class="card-comment-background">
									<img src="./assets/img/Testimoni_Art (800x).png" style="width: 100%; height: auto;">	
								</div>
								<i class="fa fa-quote-left fa-2x quote-logo"></i>
								<p style="margin-bottom: 50%; font-size: 14px;">
									Desain profesional dan Produk Berkualitas. Pelayanan yang diberikan sangat memuaskan.
								</p>
								<p class="tester">Pak Anna</p>
								<p class="company">Telkom</p>
							</div>
		                  </div>
		                </div>
		              <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
		                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		                <span class="sr-only">Previous</span>
		              </a>
		              <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
		                <span class="carousel-control-next-icon" aria-hidden="true"></span>
		                <span class="sr-only">Next</span>
		              </a>
		            </div>
		        </div>
			</div>
		</div>
	</section>

	<div class="container-fluid card-comment" style="position: relative; padding: 20% 0%; margin: 0px;">
		<div class="card-comment-background-2">
			<img src="./assets/img/Testimoni_Art (800x).png" style="width: 100%; height: auto;">	
		</div>
		<div class="container">
			<div class="row">
				<div class="col-7" style="margin-right: 8%; align-self: center;">
					<p>"Start building your integration and accept your first payment in minutes. Stripr libraries are available in every language from Ruby."</p>
					<div class="garis-3"></div>
				</div>
				<div class="col-1" style="border-radius: 100%; align-self: center; padding: 0;">
					<img src="./assets/img/Maddison.jpg" width="100%" height="auto" style="border-radius: 100%;">	
				</div>
				<div class="col-3" style="align-self: center;">
					<p style=" margin-bottom: 0px;"><b>Madison Warren</b></p>
					<p>Product Designer at UI_Expert</p>
				</div>
			</div>
		</div>	
	</div>

	<div class="container-fluid" style="border-top: solid #4288ce 1px; padding: 10% 5%;">
		<div class="container">
			<div class="row">
				<div class="col-7">
					<img src="./assets/img/Form_Art (800x).png" style="width: 100%; height: auto;">
				</div>
				<div class="col-5">
					<div class="row">
						<h4 class="judul2">Siap Berkolaborasi dengan kami?</h4>
						<p style="font-size: 15px;">Tunggu apalagi. Hubungi kami untuk diskusi lebih lanjut.</p>
					</div>
					<div class="row" style="margin-top: 15%;">
						<input class="form-control placeholder-blue" type="text" name="name" placeholder="Nama Lengkap">
						<input class="form-control placeholder-blue" type="text" name="company" placeholder="Perusahaan">
						<input class="form-control placeholder-blue" type="text" name="email" placeholder="Email">
						<input class="form-control placeholder-blue" type="text" name="handphone" placeholder="No. Hp">
						<textarea class="form-control placeholder-blue"name="pesan" placeholder="Pesan"></textarea>
					</div>
					<div class="row">
							<button class="btn btn-sawala-2" style="width: 30%;">Kirim</button>
					</div>
				</div> 
			</div>
		</div> 
	</div>
</body>
</html>