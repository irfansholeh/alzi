<?php
$args = array( 'post_type' => 'speakers','order' => 'ASC','orderby'=>'title','posts_per_page'=>-1);
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();

    $setting = pods('speakers',get_the_id());
    $bio = $setting->field('bio');

    ?>
    <div class="col-6 col-lg-2 text-center">
        <a style="cursor: pointer;">
            <img class="gray-img" src="<?= get_the_post_thumbnail_url(); ?>" width="100%" data-toggle="tooltip" data-placement="right" title="<?=$bio?>"/>
            <h6 style="font-weight:800; color:#000" class="mt-2"><?= the_title(); ?><br/><small><?= the_content(); ?></small></h6>
        </a>
    </div>
    <?php
    endwhile;
    ?>
<!-- Memanggil -->
<?php get_template_part( 'pendidikan' ); ?>
