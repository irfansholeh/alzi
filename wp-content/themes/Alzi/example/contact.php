<?php
$insert  = array(
    'post_type' => 'contact',
    'post_title'=>$_GET['name'],
    'post_content' => $_GET['message']
);
$PostID = wp_insert_post($insert);
add_post_meta($PostID,'phone',$_GET['phone']);
add_post_meta($PostID,'email',$_GET['email']);
add_post_meta($PostID,'company',$_GET['company']);
?>

<div class="container-fluid" style="padding: 10% 5%;" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-12 text-center">

                <?php
                $args = array(
                    'page_id' => '158',
                );
                $wp_query = new WP_Query( $args );
                if($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();
                    ?>
                <?php endwhile; endif; ?>
            </div>
            <div class="col-lg-5 col-12">
                <div class="row">
                    <h4 class="judul2">Mari berkolaborasi</h4>
                    <p style="font-size: 15px;">Tunggu apalagi. Hubungi kami untuk diskusi lebih lanjut.</p>
                </div>
                <form method="post" action="" id="contactForm">
                    <div class="row" id="formContact" style="">
                        <input class="form-control placeholder-blue" type="text" name="name" placeholder="Nama Lengkap" required>
                        <input class="form-control placeholder-blue" type="text" name="company" placeholder="Perusahaan" required>
                        <input class="form-control placeholder-blue" type="text" name="email" placeholder="Email" required>
                        <input class="form-control placeholder-blue" type="text" name="phone" placeholder="No. Hp" required>
                        <textarea class="form-control placeholder-blue" name="message" placeholder="Pesan" required></textarea>
                    </div>
                    <div class="row">
                        <div id="statusSubmit" class="col-12" style="padding: 0;"></div>
                        <input type="submit" class="btn btn-sawala-2" style="width: 30%;" value="Kirim">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


