<?php
require_once('inc/class-wp-bootstrap-navwalker.php');
require ('inc/wp_bootstrap_pagination.php');
require ('inc/wp_btpm_img_attac.php');
function wpbasics_theme_setup() {
    register_nav_menus( array(
        'primary' => __( 'Primary Menu' ),
    ) );}
add_action( 'after_setup_theme', 'wpbasics_theme_setup' );
add_theme_support( 'post-thumbnails' );
// Changing excerpt length
function new_excerpt_length($length) {
    return 30;
}
add_filter('excerpt_length', 'new_excerpt_length');

// Changing excerpt more
function new_excerpt_more($more) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');
add_filter('widget_text', 'do_shortcode');
?>