

<div class="container-fluid" style="background-color: #d9efff; padding-top: 5%; padding-bottom: 1%;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-3 col-12">
                    <?php
                    $args = array(
                        'page_id' => '160',
                    );
                    $wp_query = new WP_Query( $args );
                    if($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();
                        ?>

                    <img src="<?= get_the_post_thumbnail_url(); ?>" style=" width: 170px; height: auto;">

                    <p style="font-size: 14px; margin-top: 10px;"><?php the_content();?></p>
                    <?php endwhile; endif; ?>
                </div>
                <div class="col-lg-2 col-6">

                    <p class="title-product">Layanan</p>
                    <?php
                    $args = array( 'post_type' => 'layanan','order' => 'Desc','orderby'=>'title','posts_per_page'=>-1);
                    $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post();

                    $setting = pods('layanan',get_the_id());
                    $bio = $setting->field('bio');

                    ?>
                    <p style="font-size: 14px; margin: 0;"><?php the_title();?></p>
                        <?php
                    endwhile;
                    ?>
                </div>
                <div class="col-lg-2 col-6">
                    <p class="title-product">Produk</p>
                    <?php
                    $args = array( 'post_type' => 'produk','order' => 'Desc','orderby'=>'title','posts_per_page'=>-1);
                    $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post();

                        $setting = pods('layanan',get_the_id());
                        $bio = $setting->field('bio');

                        ?>
                    <p style="font-size: 14px; margin: 0;"><?php the_title();?></p>
                    <?php
                    endwhile;
                    ?>
                </div>
                <div class="col-lg-2 col-6">
                    <p class="title-product">Perusahaan</p>
                    <?php
                    $args = array( 'post_type' => 'produk','order' => 'Desc','orderby'=>'title','posts_per_page'=>-1);
                    $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post();

                    $setting = pods('layanan',get_the_id());
                    $bio = $setting->field('bio');

                    ?>
                    <p style="font-size: 14px; margin: 0;"><?php the_title();?></p>
                        <?php
                    endwhile;
                    ?>
                </div>
                <div class="col-lg-2 col-6">
                    <p class="title-product">Ikuti Kami</p>
                    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"/>
                    <a href="https://www.instagram.com/sawala.co/" target="_blank"><i class="fa fa-instagram fa-lg" style="font-size: 30px; color: #214A80; margin: 5px;"></i></a>
                    <a href="https://www.facebook.com/sawala.co/" target="_blank"><i class="fa fa-facebook fa-lg" style="font-size: 30px; color: #214A80; margin: 5px;"></i></a>
                </div>
            </div>
            <br>
        </div>
    </div>

<!--<script type="text/javascript" src="./assets/bootstrap/js/jquery-3.3.1.min.js"></script>-->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/bootstrap/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/bootstrap/js/popper.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/js/sawala.js?v=1.0"></script>
<script src="https://code.jquery.com/jquery-1.11.0.min.js" integrity="sha256-spTpc4lvj4dOkKjrGokIrHkJgNA0xMS98Pw9N7ir9oI=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js" integrity="sha256-HmfY28yh9v2U4HfIXC+0D6HCdWyZI42qjaiCFEJgpo0=" crossorigin="anonymous"></script>

<script src="<?php bloginfo('template_directory'); ?>/assets/slick/slick.min.js"></script>

<script>


    $(document).ready(function(){
        // Add smooth scrolling to all links
        $("a").on('click', function(event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function(){

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });
    });


    // this is the id of the form
</script>

</body>
</html>
