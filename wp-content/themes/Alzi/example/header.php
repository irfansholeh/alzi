<?php
/**
 * Created by PhpStorm.
 * User: Asep
 * Date: 9/4/2018
 * Time: 4:10 PM
 */
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php if   ( is_home() )     { bloginfo('name'); echo ' - '; bloginfo('description'); }
        elseif ( is_category() ) { single_cat_title(); echo ' - '; bloginfo('name'); }
        elseif ( is_single() )   { single_post_title(); echo ' - '; bloginfo('name'); }
        elseif ( is_page() )     { single_post_title(); echo ' - '; bloginfo('name'); }
        else                     { wp_title('', true); } ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="7/assets/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<!--    <link rel="stylesheet" type="text/css" href="./assets/landing.css?v=1.1">-->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/assets/css/sawala.css?v=1.2">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/slick/slick.css?v=1.2">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/slick/slick-theme.css?v=1.2">
    <?php
    $args = array(
        'page_id' => '20',
    );
    $wp_query = new WP_Query( $args );
    if($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();
    ?>
    <link rel="shortcut icon" type="image/png" href="<?= get_the_post_thumbnail_url(); ?>"/>
    <?php endwhile; endif; ?>
</head>
<?php
$args = array(
    'page_id' => '79',
);
$wp_query = new WP_Query( $args );
if($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();
?>
<body style="background-image: url(<?= get_the_post_thumbnail_url(); ?>); background-repeat: no-repeat;     background-position-x: right;
    background-position-y: top;" id="bodyTop">
<?php endwhile; endif; ?>
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <?php
        $args = array(
            'page_id' => '75',
        );
        $wp_query = new WP_Query( $args );
        if($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();
        ?>
        <a class="navbar-brand js-scroll-trigger" id="logoWeb" href="#bodyTop" style="">
            <img class="logo" src="<?= get_the_post_thumbnail_url(); ?>">
        </a>
        <?php endwhile; endif; ?>
        <button class="navbar-toggler navbar-toggler-right" id="btnBurger" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

            <i class="fa fa-bars"></i>
        </button>
        <?php wp_nav_menu(
            array(
                'menu'				=>	'primary menu',
                'theme_location'	=>	'primary',
                'depth'				=>	1,
                'container'			=>	'div',
                'container_class'	=>	'collapse navbar-collapse',
                'container_id'		=>	'navbarResponsive',
                'menu_class'		=>	'navbar-nav ml-auto',
                'fallback_cb'		=>	'WP_Bootstrap_Navwalker::fallback',
                'walker'			=>	new WP_Bootstrap_Navwalker()
            )
        ); ?>
        <!--
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#bodyTop">Beranda</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#layanan">Layanan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#client">Perusahaan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#produk">Produk</a>
                </li>
                <li class="nav-item">
                    <a href="#contact"><button class="btn btn-sawala" style="background: transparent; color: #5c6169;">Hubungi Kami</button></a>
                </li>
            </ul>
        </div>
        -->
    </div>
</nav>