
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--Alzheimer Indonesia - Jangan Maklum dengan Pikun-->
    <!-- Bootstrap CSS -->
    <link href="https://alzi.sawala.co/wordpress/wp-content/themes/Alzi/style.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://alzi.sawala.co/wordpress/wp-content/themes/Alzi/asset/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://alzi.sawala.co/wordpress/wp-content/themes/Alzi/asset/css/alzi.css?v=1.2">
    <link rel="icon" href="https://i1.wp.com/alziold.sawala.co/wp-content/uploads/2016/09/favicon.png?fit=32%2C32&amp;ssl=1" sizes="32x32">
    <title>arsip - Alzheimer Indonesia</title>
        <style>
            .dropdown-item{
                background-color:#6f3a96;
                color:#fff;
            }
            .dropdown-item:hover{
                background-color:#6f3a96;
                color:#fff;
            }
            .dropdown-item:active{
                background-color:#6f3a96;
                color:#fff;
            }
            .dropdown-menu{
                background-color:#6f3a96;
            }
        </style>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark fixed-top" style="background-color:#6f3a96;color:#fff;" id="mainNav">
    <div class="container">
                <a class="navbar-brand" href="http://localhost/alzi/"><img src="https://alzi.sawala.co/wordpress/wp-content/uploads/2019/02/7.png" alt="..." class="img" style="width:100px;"></a>
        
                <button class="navbar-toggler navbar-toggler-right" id="btnBurger" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

            <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
        <div class="menu-primary-menu-container"><ul id="menu-primary-menu" class="navbar-nav ml-auto"><li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-7" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-7 nav-item"><a title="Alzheimer &amp; Demensia" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link" id="menu-item-dropdown-7">Alzheimer &#038; Demensia</a>
<ul class="dropdown-menu" aria-labelledby="menu-item-dropdown-7" role="menu">
	<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-44" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-44 nav-item"><a title="Informasi Umum" href="https://alzi.sawala.co/wordpress/?page_id=72" class="dropdown-item">Informasi Umum</a></li>
	<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-45" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-45 nav-item"><a title="Artikel" href="#" class="dropdown-item">Artikel</a></li>
	<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-46" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-46 nav-item"><a title="Kliping" href="#" class="dropdown-item">Kliping</a></li>
</ul>
</li>
<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-8" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8 nav-item"><a title="Bantuan" href="#" class="nav-link">Bantuan</a></li>
<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-9" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-9 nav-item"><a title="Partisipasi" href="#" class="nav-link">Partisipasi</a></li>
<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-10" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10 nav-item"><a title="Edukasi &amp; Pelatihan" href="#" class="nav-link">Edukasi &#038; Pelatihan</a></li>
<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-11" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-11 nav-item"><a title="Kegiatan" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link" id="menu-item-dropdown-11">Kegiatan</a>
<ul class="dropdown-menu" aria-labelledby="menu-item-dropdown-11" role="menu">
	<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-53" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-53 nav-item"><a title="Pertemuan Caregiver" href="https://alzi.sawala.co/wordpress/?page_id=72" class="dropdown-item">Pertemuan Caregiver</a></li>
	<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-54" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-54 nav-item"><a title="World Alzheimer’s Month" href="#" class="dropdown-item">World Alzheimer’s Month</a></li>
	<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-55" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-55 nav-item"><a title="Remember Me Charity Concert" href="#" class="dropdown-item">Remember Me Charity Concert</a></li>
</ul>
</li>
<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-81" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-81 nav-item"><a title="Berita" href="https://alzi.sawala.co/wordpress/?page_id=78" class="nav-link">Berita</a></li>
<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-12" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12 nav-item"><a title="Riset" href="#" class="nav-link">Riset</a></li>
<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-13" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-13 nav-item"><a title="Kontak" href="https://alzi.sawala.co/wordpress/?page_id=90" class="nav-link">Kontak</a></li>
</ul></div>                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-search"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://alzi.sawala.co/wordpress/?page_id=85"><button type="button" class="btn btn-primary" style="color:#000000;background-color:#fdb515;border-color:#fdb515; margin-top: -7px; margin-left: 10px; font-size: 15px;">Donasi</button></a>
                </li>
                </ul>
         </div>
    </div>
</nav>

<section style="background:#f4f4f4; margin-top: 90px; color: #000">

    <div style="background-size: contain; background-repeat: no-repeat; background-position: right">
        <div class="container">
            <div class="row">
                <div class="col-lg-6" >
                    <h3 style="padding-top: 30px; padding-bottom: 20px; ">Kegiatan</h3>
                </div>
                <div class="col-lg-6 d-none d-sm-block" style="padding-top: 15px;">
                </div>
            </div>
        </div>
    </div>
</section>
<section style="padding-top: 30px; padding-bottom: 30px; background: #fff">
    <div class="container">
		<div class="row">
			<div class="form-group col-3">
				<select class="form-control" id="exampleFormControlSelect1">
				  <option>Jakarta</option>
				  <option>Bandung</option>
				  <option>Semarang</option>
				  <option>Yogyakarta</option>
				  <option>Surabaya</option>
				</select>
			</div>
			<div class="col-3">
				<button type="button" class="btn" style="background-color:#6f3a96;color:#fff;">Cari</button>
			</div>
		</div>
        <div class="row">
        <div class="col-lg-6" style="margin-bottom: 2px;">
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="https://alzheimer.ca/sites/default/files/images/national/home%20page/cm-research.jpg" class="card-img" style="width: 100%;height: 100%; object-fit: cover;border-bottom-right-radius:0px!important;border-top-right-radius:0px!important;">
					</div>
                    <div class="col-md-8">
                        <div class="card-body">
                        <a href="" style="font-size: 16px; color: #7A209B">Ngobrol bareng dengan para ahli Maret 2019</a><br/>
                        <i class="fa fa-calendar"></i> Sabtu, 5 Maret 2019<br/>
                        <i class="fa fa-map-marker"></i> Pondok Indah
                        </div>
                    </div>
                </div>
            </div>
            <br/>
        </div>
        <div class="col-lg-6" style="margin-bottom: 2px;">
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="https://alzheimer.ca/sites/default/files/images/national/home%20page/cm_living.jpg" class="card-img" style="width: 100%;height: 100%; object-fit: cover;border-bottom-right-radius:0px!important;border-top-right-radius:0px!important;">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                        <a href="" style="font-size: 16px; color: #7A209B">Menemani dengan penuh kasih sayang april 2019</a><br/>
                        <i class="fa fa-calendar"></i> Selasa, 2 april 2019<br/>
                        <i class="fa fa-map-marker"></i> Pondok Indah
                        </div>
                    </div>
                </div>
            </div>
            <br/>
        </div>
        <div class="col-lg-6" style="margin-bottom: 2px;">
            <div class="card mb-3" style="width:auto;height:auto">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="https://alzheimer.ca/sites/default/files/images/national/home%20page/cm_living.jpg" class="card-img" style="width: 100%;height: 100%; object-fit: cover;border-bottom-right-radius:0px!important;border-top-right-radius:0px!important;">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body" style="width:100%;height:100%">
                        <a href="" style="font-size: 16px; color: #7A209B">Menemani dengan penuh kasih sayang april 2019</a><br/>
                        <i class="fa fa-calendar"></i> Selasa, 2 april 2019<br/>
                        <i class="fa fa-map-marker"></i> Pondok Indah
                        </div>
                    </div>
                </div>
            </div>
            <br/>
        </div>
        <div class="col-lg-6" style="margin-bottom: 2px;">
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="http://alzi.sawala.co/wordpress/wp-content/uploads/2019/02/caregiver-1.jpg" class="card-img" style="width: 100%;height: 100%; object-fit: cover;border-bottom-right-radius:0px!important;border-top-right-radius:0px!important;">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                        <a href="" style="font-size: 16px; color: #7A209B">Pertemuan Caregiver Maret 2019</a><br/>
                        <i class="fa fa-calendar"></i> Sabtu, 2 Maret 2019<br/>
                        <i class="fa fa-map-marker"></i> Pondok Indah
                        </div>
                    </div>
                </div>
            </div>
            <br/>
        </div>
        </div>
        </div>
    </div>
</section>

<footer class="ftco-footer ftco-bg-dark ftco-section" style="padding-top: 100px">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h4 class="ftco-heading-2">Alzheimer Indonesia</h4>
                    <p>Alzheimer Indonesia is a non profit organization that aims to improve the quality life of people with Dementia / Alzheimer, their families and caregivers in Indonesia.</p>
                    <a href="#" class="fa fa-facebook ka"></a>
                    <a href="#" class="fa fa-youtube-play ka"></a>
                    <a href="#" class="fa fa-instagram ka"></a>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4 ml-md-5">
                    <h4 class="ftco-heading-2">Alzheimer dan Demensia</h4>
                    <ul class="list-unstyled">
                        <li><a href="#" class="py-2 d-block">Alzheimer</a></li>
                        <li><a href="#" class="py-2 d-block">Demensia</a></li>
                        <li><a href="#" class="py-2 d-block">Orang Dengan Demensia (ODD)</a></li>
                        <li><a href="#" class="py-2 d-block">10 Gejala Alzheimer</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h4 class="ftco-heading-2">Kegiatan</h4>
                    <ul class="list-unstyled">
                        <li><a href="#" class="py-2 d-block">Pertemuan Caregiver</a></li>
                        <li><a href="#" class="py-2 d-block">World Alzheimer’s Month</a></li>
                        <li><a href="#" class="py-2 d-block">Remember Me Charity Concert</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h4 class="ftco-heading-2">Memerlukan Bantuan?</h4>
                    <div class="block-23 mb-3">
                        <ul style="padding-left: 20px;">
                            <li><span class="text">Plaza 3 Blok E No. 2, Pondok Indah, Jakarta Selatan 12310</span></li>
                            <li><a href="#"><span class="text">+62 816 86 3136</span></a></li>
                            <li><a href="#"><span class="text">info@alzi.or.id</span></a></li>
                            <li><a href="#"><span class="text"><a href="https://www.facebook.com/AlzheimerIndonesia">Facebook Alzheimer Indonesia</a>
</span></a></li>
                            <li><a href="#"><span class="text"><a href="https://www.facebook.com/AlzheimerIndonesia">Twitter <a href="http://twitter.com/alzi_indonesia">@alzi_indonesia</a>
</a>
</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row" style="margin-top:10px;">
            <div class="col-md-12 text-center p-2" style="background-color:#290142;">

                <p style="padding-top:15px;padding-bottom:5px"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright ©<script>document.write(new Date().getFullYear());</script>2019 Yayasan Alzheimer Indonesia
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            </div>
        </div>
    </div>
</footer>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
    (function () {
        var options = {
            whatsapp: "+628112042009", // WhatsApp number
            call_to_action: "Message us", // Call to action
            position: "right", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
	function searchTable() {
    var input;
    var saring;
    var status; 
    var tbody; 
    var tr; 
    var td;
    var i; 
    var j;
    input = document.getElementById("input");
    saring = input.value.toUpperCase();
    tbody = document.getElementsByTagName("tbody")[0];;
    tr = tbody.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td");
        for (j = 0; j < td.length; j++) {
            if (td[j].innerHTML.toUpperCase().indexOf(saring) > -1) {
                status = true;
            }
        }
        if (status) {
            tr[i].style.display = "";
            status = false;
        } else {
            tr[i].style.display = "none";
        }
    }
}
</script>
</body>
</html>