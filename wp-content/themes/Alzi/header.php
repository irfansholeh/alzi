<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
<!--Alzheimer Indonesia - Jangan Maklum dengan Pikun-->

    <script src="<?=get_template_directory_uri();?>/assets/js/jquery-3.2.1.min.js"></script>
    <script src="<?=get_template_directory_uri();?>/assets/js/jquery-3.2.1.slim.min.js"></script>
    <script src="<?=get_template_directory_uri();?>/assets/js/popper.min.js"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?=get_template_directory_uri();?>/assets/style.css">
    <link rel="stylesheet" href="<?=get_template_directory_uri();?>/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=get_template_directory_uri();?>/assets/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?=get_template_directory_uri();?>/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=get_template_directory_uri();?>/assets/datatables/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?=get_template_directory_uri();?>/assets/select2/dist/css/select2.min.css">
    <link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/alzi.css?v=1.2">
    <link rel="icon" href="https://i1.wp.com/alziold.sawala.co/wp-content/uploads/2016/09/favicon.png?fit=32%2C32&amp;ssl=1" sizes="32x32">
    <title><?php if   ( is_home() )     { bloginfo('name'); echo ' - '; bloginfo('description'); }
        elseif ( is_category() ) { single_cat_title(); echo ' - '; bloginfo('name'); }
        elseif ( is_single() )   { single_post_title(); echo ' - '; bloginfo('name'); }
        elseif ( is_page() )     { single_post_title(); echo ' - '; bloginfo('name'); }
        else { wp_title('', true); } ?></title>

        <style>
        body {
            font-family: 'Open Sans'!important;
        }
        .fto{
            width: 100%;
            height: 100%;
        }
            .dropdown-item{
                background-color:#6f3a96;
                color:#fff;
            }
            .dropdown-item:hover{
                background-color:#6f3a96;
                color:#fff;
            }
            .dropdown-item:active{
                background-color:#6f3a96;
                color:#fff;
            }
            .dropdown-menu{
                background-color:#6f3a96;
            }
			.tmbl:hover{
				background-color:#000;
			}
		
        </style>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark fixed-top" style="background-color:#6f3a96;color:#fff;" id="mainNav">
    <div class="container">
        <a class="navbar-brand" href="<?=site_url()?>"><img src="https://www.alzi.or.id/wp-content/uploads/2016/09/logo-header.png" alt="..." class="img" style="width:100px;"></a>
        
        <button class="navbar-toggler navbar-toggler-right" id="btnBurger" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

            <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
        <?php wp_nav_menu(
            array(
                'menu'				=>	'primary menu',
                'theme_location'	=>	'primary',
                'depth'				=>	2,
                'container'			=>	'div',
                'menu_class'		=>	'navbar-nav ml-auto',
                'fallback_cb'		=>	'WP_Bootstrap_Navwalker::fallback',
                'walker'			=>	new WP_Bootstrap_Navwalker()
            )
        ); ?>
                 <li class="nav-item" >
                    <!--<a class="nav-link" href="#"><i class="fa fa-search"></i></a>-->
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=site_url()?>/donasi"><button type="button" class="btn btn-primary" style="color: #000000;background-color:#fdb515;border-color:#fdb515; margin-top: -7px; margin-left: 10px; font-size: 15px;">Donasi</button></a>
                </li>
                </ul>
                </div>
    </div>
</nav>

