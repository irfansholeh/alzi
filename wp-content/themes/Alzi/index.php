<?php
get_header();
?>

<div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="margin-top: 90px;">
    <div class="carousel-inner">
            <?php
            $args = array(
                'post_type' => 'slider',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'kategory_alzi',
                        'field'    => 'slug',
                        'terms'    => array('desktop'),
                    ),
                ),
            );
            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post();
            $setting = pods('kategory_alzi',get_the_id());
            ?>

             <?php
                $featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');
             ?>
            <div class="carousel-item d-none d-md-block d-lg-none d-lg-block d-xl-none d-xl-block">
                <img class="d-none d-sm-block w-100" src="<?=$featured_img_url?>" alt="<?=the_title();?>">
            </div>
            <?php
            endwhile;
            ?>
            <?php
            $args = array(
                'post_type' => 'slider',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'kategory_alzi',
                        'field'    => 'slug',
                        'terms'    => array('mobile'),
                    ),
                ),
            );
            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post();
            $setting = pods('kategory_alzi',get_the_id());
            ?>
            <?php
            $featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');
            ?>
            <div class="carousel-item d-block d-sm-none d-none d-sm-block d-md-none">
                <img class="d-block d-sm-block w-100" src="<?=$featured_img_url?>" alt="First slide">
            </div>
            <?php
            endwhile;
            ?>

    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<section style="background:#fcfcfc; padding-top: 70px; padding-bottom: 70px;">

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3 style="font-weight: 600; margin-bottom: 20px;">Saya adalah..</h3>
            </div>
        </div>
    </div>

    <!--ffd6ff-->
    <div class="container d-none d-xl-block d-lg-block d-xl-none">
        <div class="row">
            <?php
            $args = array( 'post_type' => 'saya_adalah','order' => 'DESC','orderby'=>'title','posts_per_page'=>-1);
            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post();
                $setting = pods('saya_adalah',get_the_id());
                $featured_content = $setting->field('featured_content');
                ?>
            <div class="col-lg-4 col-12 d-flex">
                <div class="card box-alzi w-100 mb-5" style="border-color:#6f3a96;">
                    <div style="background: #6F3A96;padding: 20px 40px;color: #fff;">
                        <a href="<?=the_permalink()?>" ><h4 class="card-head-alzi text-white"><?= the_title(); ?><i class="fa fa-arrow-right pull-right"></i></h4></a></div>
                    <div class="card-body">
                        <?php foreach ($featured_content as $row): 
                            $id = $row['ID'];
                        ?>
                            <li style="list-style: none;"><a href="<?=get_the_permalink($id)?>"><?= $row['post_title']; ?></a></li>
                        <?php endforeach ?>
                    </div>
                </div><br/>
            </div>
            <?php
            endwhile;
            ?>
    </div>
    </div>

    <!--Mobile only-->
    <div class="container mobileSayaAdalah d-block d-sm-none d-sm-block d-md-none d-md-block d-lg-none">
        <div class="accordion" id="accordionExample">
            <?php
            $args = array( 'post_type' => 'saya_adalah','order' => 'DESC','orderby'=>'title','posts_per_page'=>-1);
            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post();
                $setting = pods('saya_adalah',get_the_id());
                $featured_content = $setting->field('featured_content');
                $id = get_the_ID();
                //$bio = $setting->field('bio');
                ?>
            <div class="card">
                <div class="card-header" id="headingTwo" style="background-color:#6f3a96;">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#<?php echo $id;?>" aria-expanded="true" aria-controls="collapseTwo" style="color:#fff;">
                            <?= the_title(); ?>
                            <i class="fa fa-chevron-down pull-right" color="#fff;" style=""></i>
                        </button>
                    </h2>
                </div>

                <div id="<?php echo $id;?>" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                       <?php foreach ($featured_content as $row): ?>
                            <?php
                                $id = $row['ID'];
                            ?>
                            <li style="list-style: none;"><a href="<?=the_permalink($id)?>"><?= $row['post_title']; ?></a></li>
                        <?php endforeach ?>
                        <a href="<?=the_permalink()?>"><div class="pull-right" style="color:#542577">Detail <i class="fas fa-arrow-right " style="color:#542577;"></i></a>
                    </div><br>
                </div>
            </div>
            <?php
            endwhile;
            ?>
        </div>
    </div>
</section>
<section style="background: #cccccc38; padding-top: 100px; padding-bottom: 100px">

    <div class="container">
        <div class="row justify-content-center">
            <div class="embed-responsive embed-responsive-16by9 col-8">
                <iframe class="embed-responsive-item" src="https://www.google.com/maps/d/embed?mid=1av8u7nLCjXsFksf6eFpiRNlOOhpjd6Ee" allowfullscreen></iframe>
            </div>
        </div>
        <br><br>
        <div class="row justify-content-center">
        <div class="col-lg-3">
           <a href="http://alzi.sawala.co/new/chapter/" class="btn btn-primary btn-lg" style="margin-top: 10px; background: #7a219a; border-color: #7a219a; color:#ffffff;width:100%">Lihat Semua</a>
       </div>
       </div>
    </div>
</section>
<section style="background: #fcfcfc; padding-top: 100px; padding-bottom: 100px">

    <div class="container">
        <div class="row" style=" display: flex; flex-wrap: wrap;">

            <div class="col-12">
                <h3 style="font-weight: 600">Kegiatan</h3>
                <p>Berikut diantaranya kegiatan dari Alzi yang rutin diselenggarakan.</p><br/>
            </div>
    <?php
        $args = array( 'post_type' => 'inti_kegiatan','order' => 'Desc','orderby'=>'title','posts_per_page'=>3);
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post();
        $setting = pods('inti_kegiatan',get_the_id());
        //$bio = $setting->field('bio');
    ?>
            <div class="col-lg-4 col-12" style="display: flex;flex-direction: column;border-color:#6f3a96;">
                <div class="card box-alzi" style="flex: 1;border-color: #6f3a96;">
                    <img class="card-img-top" src="<?= get_the_post_thumbnail_url(); ?>" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title" style="font-weight: 600"><a href="<?php the_permalink(); ?>"><?= the_title(); ?></a></h5>
                        <hr style="border-color:#6f3a96;">
                        <p class="card-text" style="font-size: 14px"><?= the_excerpt(); ?></p>
                    </div>
                    <div class="card-footer text-center" style="background-color: #6f3a96;color:#fff;">
                       <b style="text-align: center;"><a href="<?php the_permalink(); ?>" style="color:#fff;">Detail</a></b>
                    </div>
                </div><br/>

            </div>
    <?php
    endwhile;
    ?>
  </div>
    </div>
</section>
<section style="padding-top: 100px; padding-bottom: 60px; background: #cccccc38">
    <div class="container">
        <h3 style="font-weight: 600">Kegiatan Mendatang</h3>
        <p>Berikut diantaranya kegiatan mendatang dari Alzi yang rutin diselenggarakan.</p><br>
        <div class="row justify-content-center" style="display: flex; flex-wrap: wrap;">
            <?php
            $args = array( 'post_type' => 'kegiatan','order' => 'asc','orderby'=>'title','posts_per_page'=>4);
            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post();
                $setting = pods('kegiatan',get_the_id());
                $tgl = $setting->field('jadwal');
                $lks = $setting->field('lokasi');
            ?>
            <div class="col-12 col-md-6 col-lg-6 col-xl-6 mb-4"  style="display: flex;flex-direction: column;">
                <div class="row no-gutters" style="flex: 1;border-radius: 10px; background-color: white;">
                    <div class="col-4 bgcolor">
                        <img src="<?= get_the_post_thumbnail_url(); ?>" alt="image" style="width: 100%;height: 100%; object-fit: cover;border-bottom-right-radius:0px!important;border-top-right-radius:0px!important;border: 2px solid #7a219a; border-radius: 10px; border-right: 0px;">
                    </div>
                    <div class="col-2" style="padding: 10px;padding-top: 17px; border-top: 2px solid #7a219a; border-bottom: 2px solid #7a219a;">
                        <center><h4 class="display-5"><span class="badge " style="background-color:#6f3a96;color:#fff"><?php echo date("d", strtotime($tgl));?></span></h4>
                        <h6 style="text-shadow:2px 2px 1px #fff; font-weight: bold; font-size: 18px; text-transform: uppercase;" ><?php $tanggal = date("F", strtotime($tgl)); echo substr($tanggal,0,3);?></h6>
                        </center>
                    </div>
                    <div class="col-6" style="padding: 10px;padding-top: 17px; border-bottom-left-radius:0px!important;border-top-left-radius:0px!important;border: 2px solid #7a219a; border-radius: 10px; border-left: 0px;">
                        <a href="<?php the_permalink(); ?>"><h6 style="color:#6f3a96"><?=the_title();?></h6></a>
                        <small>
                            <ul class="list-inline">
                                <li class="list-inline-item"><i class="fa fa-home" aria-hidden="true"></i> <?=$lks?></li>
                            </ul>
                        </small>
                    </div>
                </div>
            </div>
            <?php
            endwhile;
            ?>
        <div class="col-12"><br/><br/>
                <a href="<?=site_url()?>/kegiatan" class="btn btn-primary" style="background: #7a219a; border-color: #7a219a; color:#ffffff">Lihat Semua Kegiatan</a>
            </div>
        </div>
        </div>
    </div>
</section>
<section style="background: #7a219a; padding-top: 100px; padding-bottom: 100px">
    <div class="container">
        <div class="row ">
            <div class="col-12 text-center">
                <h3 class="text-white"><span style="font-weight: 600">Mari berpartisipasi</span><br/><small>Untuk mengatasi Alzheimer & Demensia</small></h3><br/>
                <div class="row">
                    <div class="col-lg-4 col-12">
                        <a href="<?=site_url()?>/donasi" style="border-radius: 30px; margin-bottom: 15px; font-size: 18px; padding-left: 100px; padding-right: 100px; border-color: #ffffff!important; color: #ffffff" class="btn btn-lg btn-outline-primary btn-cta-alzi btn-block"><i class="fa fa-dollar"></i> Berdonasi</a>
                    </div>
                    <div class="col-lg-4 col-12">
                        <a href="" style="border-radius: 30px; margin-bottom: 15px;  font-size: 18px; padding-left: 100px; padding-right: 100px; border-color: #ffffff!important; color: #ffffff" class="btn btn-lg btn-outline-primary btn-cta-alzi btn-block"><i class="fa fa-user"></i> Menjadi Relawan</a>
                    </div>
                    <div class="col-lg-4 col-12">
                        <a href="<?=site_url()?>/kegiatan" style="border-radius: 30px; margin-bottom: 15px; font-size: 18px; padding-left: 100px; padding-right: 100px; border-color: #ffffff!important; color: #ffffff" class="btn btn-lg btn-outline-primary btn-cta-alzi btn-block"><i class="fa fa-calendar"></i> Ikut Kegiatan</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
 .bgcolor {

 }
</style>
<?php
get_footer();
?>
