<?php
/* Template Name: Tentang */ 

get_header();
?>
<section style="background:#f4f4f4; margin-top: 90px; color: #000">

    <div style="background-size: contain; background-repeat: no-repeat; background-position: right">
        <div class="container">
            <div class="row">
                <div class="col-lg-6" >
                    <h3 style="padding-top: 30px; padding-bottom: 20px; "> Tentang</h3>
                </div>
                <div class="col-lg-6 d-none d-sm-block" style="padding-top: 10px;">
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container" style=" padding-bottom: 120px; padding-top:50px;" >
    <div class="row">
        <div class="col-lg-8">
            <article id="post-85" class="post-85 page type-page status-publish hentry">
					<div class="entry-content">
                        <?php
                        $args = array(
                            'page_id' => '74',
                        );
                        $wp_query = new WP_Query( $args );
                        if($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post();
                        ?>
                        <?= the_content(); ?>
                        <?php endwhile; endif; ?>
                    </div> <!-- .entry-content -->
            </article> <!-- .et_pb_post -->
        <br/><br/>
</div>
    <div class="col-lg-4" style="padding-left: 50px;">
    <?php get_template_part( 'sidebar' ); ?>
    </div>
    </div>
</div>
    </div>
<style>
    
    .card {
    box-shadow: 0px 1px 2px 0px #e4e6e8;
    -webkit-box-shadow: 0px 1px 4px 0px #e4e6e8;
    -moz-box-shadow: 0px 1px 4px 0px #e4e6e8;
    margin:20px;
}
</style>

<?php
get_footer();
?>