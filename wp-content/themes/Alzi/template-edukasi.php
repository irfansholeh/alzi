<?php 
/* Template Name: Edukasi & Pelatihan */ 

get_header();
?>
<section style="background:#9254bf; margin-top: 90px; color: #ffffff;margin-color:100px">

    <div style="background-size: contain; background-repeat: no-repeat; background-position: right;background-color:#f4f4f4">
        <div class="container">
            <div class="row">
                <div class="col-lg-6" >
                    <h3 style="padding-top: 30px; padding-bottom: 20px;color:#000">Edukasi & Pelatihan</h3>
                </div>
                <div class="col-lg-6 d-none d-sm-block" style="padding-top: 15px;">
                </div>
            </div>
        </div>
    </div>
</section>
<section style="background: #fff; padding-top: 30px; padding-bottom: 30px">
    <div class="container">
        <div class="row" style=" display: flex; flex-wrap: wrap;">
            <?php
                $args = array( 'post_type' => 'post','category_name' => 'edukasipelatihan');
                $loop = new WP_Query( $args );
                while ( $loop->have_posts() ) : $loop->the_post();

                $setting = pods('blog',get_the_id());
              ?>
            <div class="col-lg-4 col-12" style="display: flex;flex-direction: column;">
                <div class="card box-alzi" style="flex: 1;">
                    <img class="card-img-top" src="<?= get_the_post_thumbnail_url(); ?>" alt="Card image cap" width="auto" height="auto">
                    <div class="card-body">
                        <h5 class="card-title" style="font-weight: 600"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h5>
                        <p>
                            <?php $content = get_the_content();
                            $trimmed_content = wp_trim_words( $content,'50',true); ?>
                            <?php echo $trimmed_content; ?>
                        </p>
                        <p class="post-meta text-muted" style="padding-top:15px;font-size:14px"><?php the_time('d/m/Y'); ?></span> | <a href="#" rel="category tag" style="color:#9756c7;"><?php the_category(', '); ?></a></span></p>
                        <p class="card-text" style="font-size: 14px"><?php the_excerpt(); ?></p>
                    </div>
                </div><br/>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
<?php
get_footer();
?>