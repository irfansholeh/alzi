<?php
get_header();
the_post();
?>
<section style="background:#f4f4f4; margin-top: 90px; color: #000">

    <div style="background-size: contain; background-repeat: no-repeat; background-position: right">
        <div class="container">
            <div class="row">
                <div class="col-lg-6" >
                    <h3 style="padding-top: 30px; padding-bottom: 20px; "> <?=the_title(); ?></h3>
                </div>
                <div class="col-lg-6 d-none d-sm-block" style="padding-top: 10px;">
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container" style=" padding-bottom: 120px; padding-top:50px;" >
    <div class="row">
        <div class="col-lg-8">
            <article id="post-85" class="post-85 page type-page status-publish hentry">
					<div class="entry-content">
                        
                        
                        <?=the_content(); ?>
                        
                        <?php wp_reset_postdata(); ?>
                       
                    </div> <!-- .entry-content -->
            </article> <!-- .et_pb_post -->
        <br/><br/>
</div>
    <div class="col-lg-4" style="padding-left: 50px;">
    <?php get_template_part( 'sidebar' ); ?>
    </div>
    </div>
</div>
    </div>
<style>
    
    .card {
    box-shadow: 0px 1px 2px 0px #e4e6e8;
    -webkit-box-shadow: 0px 1px 4px 0px #e4e6e8;
    -moz-box-shadow: 0px 1px 4px 0px #e4e6e8;
    margin:20px;
}
</style>

<?php

get_footer();
?>