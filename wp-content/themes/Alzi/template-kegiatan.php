<?php 
/* Template Name: Kegiatan */

get_header();
?>
<section style="background:#9254bf; margin-top: 90px; color: #ffffff;">
 <div style="background-size: contain; background-repeat: no-repeat; background-position: right;background-color:#f4f4f4;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6" >
                    <h3 style="padding-top: 30px; padding-bottom: 20px;color:#000">Kegiatan</h3>
                </div>
                <div class="col-lg-6 d-none d-sm-block" style="padding-top: 15px;">
                </div>
            </div>
        </div>
    </div>
</section>
<section style="padding-top: 30px; padding-bottom: 30px; background: #ffff;margin-bottom:40px">

    <div class="container">
        <form method="GET" action="<?=site_url()?>">
        <div class="row justify-content-start">
            <div class="col-12 col-md-2 col-xl-2 col-lg-2" style="margin:5px">
                <select class="form-control" name="kategori_event" id="exampleFormControlSelect1">
                    <option value="">Pilih Kategori</option>
                    <?php
                        $terms = get_terms('kategori_event' );
                        $termsName=array();
                        foreach ( $terms as $term ):
                            ?>
                        <option value="<?=$term->slug;?>"><?=$term->name;?></option>
                    <?php
                        endforeach;
                    ?>
                </select>
            </div>

            <div class="col-12 col-md-2 col-xl-2 col-lg-2" style="margin:5px">
                <select class="form-control" name="bulan" id="exampleFormControlSelect1">
                    <option value="">Pilih Bulan</option>
                    <option value="01">January</option>
                    <option value="02">Februari</option>
                    <option value="03">Maret</option>
                    <option value="04">April</option>
                    <option value="05">Mei</option>
                    <option value="06">Juni</option>
                    <option value="07">Juli</option>
                    <option value="08">Agustus</option>
                    <option value="09">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">November</option>
                    <option value="12">Desember</option>
                </select>
            </div>

            <div class="col-12 col-md-2 col-xl-2 col-lg-2" style="margin:5px">
                <input type="text" class="form-control" name="tahun">
            </div>

            <div class="col-6 col-md-1 col-xl-1 col-lg-1" style="margin:5px">
                <button type="submit" class="btn btn-warning" style="width: 100%">Cari</button>
            </div>
        </div>
            <input type="hidden" name="page_id" value="66">
        </form>
        <br>
        <div class="row" style="display: flex; flex-wrap: wrap;">

            <?php
            if(empty($_GET['kategori_event'])) {
                $args = array(
                    'post_type' => 'kegiatan'
                );
            } else {
                $args = array(
                    'post_type' => 'kegiatan',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'kategori_event',
                            'field'    => 'slug',
                            'terms'    => $_GET['kategori_event'],
                        ),
                    ),
                );
            }

            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post();
                $setting = pods('kegiatan',get_the_id());
                $tgl = $setting->field('jadwal');
                $lks = $setting->field('lokasi');
                $pecahkan = explode('-', $tgl);
                ?>
                <?php
                    if (isset($_GET['bulan']) || isset($_GET['tahun'])):
                        $bulan = $_GET['bulan'];
                        $tahun = $_GET['tahun'];
                        if ($bulan == $pecahkan[1] || $tahun == $pecahkan[0]): ?>
                        <div class="col-12 col-md-6 col-lg-6 col-xl-6 mb-4"  style="display: flex;flex-direction: column;">
                            <div class="row no-gutters" style="flex: 1;border-radius: 10px;">
                                <div class="col-4 bgcolor">
                                    <img src="<?= get_the_post_thumbnail_url(); ?>" alt="image" style="width: 100%;height: 100%; object-fit: cover;border-bottom-right-radius:0px!important;border-top-right-radius:0px!important;border: 2px solid #7a219a; border-radius: 10px; border-right: 0px;">
                                    <!-- <div style="padding: 50% 0px;">
                                    <center><h5 class="display-5"><span class="badge " style="background-color:#6f3a96;color:#fff"><?php echo date("d", strtotime($tgl));?></span></h5></center>
                                    <h6 style="text-shadow:2px 2px 1px #fff; font-weight: bold; font-size: 18px; text-transform: uppercase;" ><?php echo date("F", strtotime($tgl));?></h6>
                                    </div> -->
                                </div>
                                <div class="col-2" style="padding: 10px;padding-top: 17px; border-top: 2px solid #7a219a; border-bottom: 2px solid #7a219a;">
                                    <center><h4 class="display-5"><span class="badge " style="background-color:#6f3a96;color:#fff"><?php echo date("d", strtotime($tgl));?></span></h4>
                                    <h6 style="text-shadow:2px 2px 1px #fff; font-weight: bold; font-size: 18px; text-transform: uppercase;" ><?php $tanggal = date("F", strtotime($tgl)); echo substr($tanggal,0,3);?></h6>
                                    </center>
                                </div>
                                <div class="col-6" style="padding: 10px;padding-top: 17px; border-bottom-left-radius:0px!important;border-top-left-radius:0px!important;border: 2px solid #7a219a; border-radius: 10px; border-left: 0px;">
                                    <a href="<?php the_permalink(); ?>"><h6 style="color:#6f3a96"><?=the_title();?></h6></a>
                                    <small>
                                        <ul class="list-inline">
                                            <li class="list-inline-item"><i class="fa fa-home" aria-hidden="true"></i> <?=$lks?></li>
                                        </ul>
                                         <?php
                                        $terms = get_the_terms( $post->ID , 'kategori_event' );
                                        $termsName=array();
                                        foreach ( $terms as $term ):
                                            ?>
                                            <span class="badge badge-pill badge-large text-dark" style="background-color: #c1c1c1;"><?=$term->name?></span>
                                        <?php endforeach;  ?>
                                    </small>
                                </div>
                            </div>
                        </div>
                        <?php 

                        elseif ($bulan == null && $tahun == null): ?>
                            <div class="col-12 col-md-6 col-lg-6 col-xl-6 mb-4"  style="display: flex;flex-direction: column;">
                                <div class="row no-gutters" style="flex: 1;border-radius: 10px;">
                                    <div class="col-4 bgcolor">
                                        <img src="<?= get_the_post_thumbnail_url(); ?>" alt="image" style="width: 100%;height: 100%; object-fit: cover;border-bottom-right-radius:0px!important;border-top-right-radius:0px!important;border: 2px solid #7a219a; border-radius: 10px; border-right: 0px;">
                                        <!-- <div style="padding: 50% 0px;">
                                        <center><h5 class="display-5"><span class="badge " style="background-color:#6f3a96;color:#fff"><?php echo date("d", strtotime($tgl));?></span></h5></center>
                                        <h6 style="text-shadow:2px 2px 1px #fff; font-weight: bold; font-size: 18px; text-transform: uppercase;" ><?php echo date("F", strtotime($tgl));?></h6>
                                        </div> -->
                                    </div>
                                    <div class="col-2" style="padding: 10px;padding-top: 17px; border-top: 2px solid #7a219a; border-bottom: 2px solid #7a219a;">
                                        <center><h4 class="display-5"><span class="badge " style="background-color:#6f3a96;color:#fff"><?php echo date("d", strtotime($tgl));?></span></h4>
                                        <h6 style="text-shadow:2px 2px 1px #fff; font-weight: bold; font-size: 18px; text-transform: uppercase;" ><?php $tanggal = date("F", strtotime($tgl)); echo substr($tanggal,0,3);?></h6>
                                        </center>
                                    </div>
                                    <div class="col-6" style="padding: 10px;padding-top: 17px; border-bottom-left-radius:0px!important;border-top-left-radius:0px!important;border: 2px solid #7a219a; border-radius: 10px; border-left: 0px;">
                                        <a href="<?php the_permalink(); ?>"><h6 style="color:#6f3a96"><?=the_title();?></h6></a>
                                        <small>
                                            <ul class="list-inline">
                                                <li class="list-inline-item"><i class="fa fa-home" aria-hidden="true"></i> <?=$lks?></li>
                                            </ul>
                                             <?php
                                            $terms = get_the_terms( $post->ID , 'kategori_event' );
                                            $termsName=array();
                                            foreach ( $terms as $term ):
                                                ?>
                                                <span class="badge badge-pill badge-large text-dark" style="background-color: #c1c1c1;"><?=$term->name?></span>
                                            <?php endforeach;  ?>
                                        </small>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    
                    <?php endif ?>
            <?php
            endwhile;
            ?>
        </div>
        </div>
    </div>
</section>

<?php
get_footer();
?>
