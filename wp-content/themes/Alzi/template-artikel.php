<?php 
/* Template Name: Artikel */ 

get_header();
?>
<section style="background:#9254bf; margin-top: 90px; color: #ffffff;margin-color:100px">

    <div style="background-size: contain; background-repeat: no-repeat; background-position: right;background-color:#f4f4f4">
        <div class="container">
            <div class="row">
                <div class="col-lg-6" >
                    <h3 style="padding-top: 30px; padding-bottom: 20px;color:#000">Artikel</h3>
                </div>
                <div class="col-lg-6 d-none d-sm-block" style="padding-top: 15px;">
                </div>
            </div>
        </div>
    </div>
</section>
<section style="background: #fff; padding-top: 30px; padding-bottom: 30px">
    <div class="container">
        <div class="row" style=" display: flex; flex-wrap: wrap;">
            <?php query_posts('category_name=Artikel'); ?>
            <?php while ( have_posts() ) : the_post(); ?>
            <div class="col-lg-4 col-12" style="display: flex;flex-direction: column;">
                <div class="card box-alzi" style="flex: 1;">
                    <img class="card-img-top" src="<?= get_the_post_thumbnail_url(); ?>" alt="Card image cap" width="auto" height="auto">
                    <div class="card-body">
                        <h5 class="card-title" style="font-weight: 600"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h5>
                        	<p class="post-meta text-muted" style="padding-top:15px;font-size:14px"><?php the_time('d/m/Y'); ?></span> | <a href="#" rel="category tag" style="color:#9756c7;"><?php the_category(', '); ?></a></span></p>
                        <p class="card-text" style="font-size: 14px"><?php the_excerpt(); ?></p>
                    </div>
                </div><br/>
            </div>
            <?php endwhile; ?>
            <?php wp_reset_query(); ?>
        </div>
    </div>
</section>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
<?php
get_footer();
?>