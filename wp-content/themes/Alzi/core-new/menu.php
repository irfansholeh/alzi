<nav class="navbar navbar-expand-lg navbar-dark fixed-top" style="background-color:#6f3a96;color:#fff;">
    <div class="container">
        <a class="navbar-brand" href="<?php echo base_url('welcome/index');?>"><img src="<?=base_url()?>asset/images/7.png" alt="..." class="img" style="width:100px;"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto" style="margin-top: 10px;">
                <li class="nav-item ">
                    <a class="nav-link" href="#">Alzheimer & Demensia</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" >Bantuan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" >Partisipasi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Edukasi & Pelatihan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Kegiatan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Riset</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Kontak</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-search"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><button type="button" class="btn btn-primary" style="color:#000000;background-color:#fdb515;border-color:#fdb515; margin-top: -7px; margin-left: 10px; font-size: 15px;">Donasi</button></a>
                </li>

        </div>
        <!--<li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Dropdown link
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>-->
        </ul>
    </div>
</nav>
