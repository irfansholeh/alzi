<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="<?=base_url()?>asset/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url("asset/css/alzi.css?v=1.2")?>">
    <link rel="icon" href="https://i1.wp.com/alziold.sawala.co/wp-content/uploads/2016/09/favicon.png?fit=32%2C32&amp;ssl=1" sizes="32x32">
    <title><!--Alzheimer Indonesia - Jangan Maklum dengan Pikun--><?php if   ( is_home() )     { bloginfo('name'); echo ' - '; bloginfo('description'); }
        elseif ( is_category() ) { single_cat_title(); echo ' - '; bloginfo('name'); }
        elseif ( is_single() )   { single_post_title(); echo ' - '; bloginfo('name'); }
        elseif ( is_page() )     { single_post_title(); echo ' - '; bloginfo('name'); }
        else                     { wp_title('', true); } ?></title>
</head>
<body>