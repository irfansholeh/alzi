<?php
require_once('inc/class-wp-bootstrap-navwalker.php');
require ('inc/wp_bootstrap_pagination.php');
require ('inc/wp_btpm_img_attac.php');
function wpbasics_theme_setup() {
    register_nav_menus( array(
        'primary' => __( 'Primary Menu' ),
    ) );}
add_action( 'after_setup_theme', 'wpbasics_theme_setup' );
add_theme_support( 'post-thumbnails' );
// Changing excerpt length
function new_excerpt_length($length) {
    return 30;
}
add_filter('excerpt_length', 'new_excerpt_length');

// Changing excerpt more
function new_excerpt_more($more) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');
add_filter('widget_text', 'do_shortcode');
/*
* Define a constant path to our single template folder
*/
// Function add class to content area
function add_class_content($content){
    return str_replace(
    	array('<img class="', 'alignleft', 'alignright', 'aligncenter', '<p>', '<li>'),
    	array('<img class="img-fluid img-thumbnail ', 'float-left', 'float-right', 'mx-auto d-block', '<p class="text-justify">', '<li class="text-justify">'),
    	$content
    );
}
add_filter('the_content','add_class_content');
function add_class_the_tags($html){
	return str_replace('<a', '<a class="text-white"', $html);
}
add_filter('the_tags', 'add_class_the_tags');
 
// Function add class to comments
require ('inc/wp_comment_walker.php');
function add_class_reply($link, $args, $comment, $post){
	return str_replace("class='comment-reply-link'", "class='comment-reply-link btn btn-outline-primary btn-sm'", $link);
}


?>