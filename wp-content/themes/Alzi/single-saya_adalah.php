<?php get_header();
?>
<section style="background:#9254bf; margin-top: 90px; color: #ffffff;">
    <div style="background-size: contain; background-repeat: no-repeat; background-position: right;background-color:#f4f4f4;">
        <div class="container">
            <div class="row">
                <div class="col-lg-6" >
                    <?php 
                    if (have_posts()) :
                        while ( have_posts()) :
                            the_post();
                    ?>
                    <h3 style="padding-top: 30px; padding-bottom: 20px;color:#000"><?=the_title()?></h3>
                    <?php
                        endwhile;
                    endif;
                    ?>
                </div>
                <div class="col-lg-6 d-none d-sm-block" style="padding-top: 15px;">
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container" style=" padding-bottom: 120px; padding-top:50px;" >
    <div class="row" style=" display: flex; flex-wrap: wrap;">
            <?php
            if (have_posts()) :
                while ( have_posts()) :
                    the_post();
                    $setting = pods('saya_adalah',get_the_ID());
                    $content_category = $setting->field('content_category');
                    foreach ($content_category as $row) {
                        $id = $row["term_id"]; 
                    }
            ?>
            <?php
            $args = array( 'post_type' => 'post','order' => 'DESC','cat' => $id);
            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post();
            ?>
            <div class="col-lg-4 col-12" style="display: flex;flex-direction: column;">
                <div class="card box-alzi" style="flex: 1;">
                    <?php if (!empty(get_the_post_thumbnail_url())): ?>
                        <img class="card-img-top" src="<?= get_the_post_thumbnail_url(); ?>" alt="Card image cap" width="auto" height="auto">
                    <?php endif ?>
                    <div class="card-body">
                        <h5 class="card-title" style="font-weight: 600"><a href="<?php the_permalink();?>"><?php the_title();?></a></h5>
                            <p class="post-meta text-muted" style="padding-top:15px;font-size:14px"><?php the_time('d/m/Y'); ?></span> | <a href="#" rel="category tag" style="color:#9756c7;"><?php the_category(', '); ?></a></span></p>
                        <p class="card-text" style="font-size: 14px"><?php the_excerpt(); ?></p>
                    </div>
                </div><br/>
            </div>
            <?php
            endwhile;
            ?>
        <?php
                endwhile;
            endif;
        ?>
    </div>
    <Br>
</div>
<?php
get_footer();
?>
