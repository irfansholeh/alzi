<?php 
/* Template Name: Chapter */ 
get_header();
?>
    <section style="background:#f6f6f6; margin-top: 90px; color: #000">
		<div style="background-size: contain; background-repeat: no-repeat; background-position: right">
			<div class="container">
				<div class="row">
					<div class="col-lg-6" >
						<h3 style="padding-top: 30px; padding-bottom: 20px; ">Chapter</h3>
					</div>
					<div class="col-lg-6 d-none d-sm-block" style="padding-top: 15px;">
					</div>
				</div>
			</div>
		</div>
	</section>
	<section style="background: #fcfcfc; padding-top: 30px; padding-bottom: 30px">
		<div class="container">
			<div class="row justify-content-center" style=" display: flex; flex-wrap: wrap;">
                <ul class="list-group col-md-9 col-12 col-md-9">
                    <?php
                    $terms = get_terms('kategori_event' );
                    foreach ( $terms as $term ):
                        if ($term->parent == 14):
                    ?>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <h3 style="color:#6f3a96"><?php $explode = explode(' ',($term->name)); echo $explode[1];?></h3>
                        <a href="<?=site_url()?>/kegiatan/?kategori_event=<?=$term->slug;?>" ><i class="fa fa-tag" style="color:#6f3a96;"></i></a>
                    </li>
                <?php
                    endif;
                    endforeach;
                ?>
                <ul>
			</div>
		</div>
	</section>
<?php  
get_footer();
?>