<?php /* Template Name: Kontak */ ?>
<?php
get_header();
?>
<br><br><br>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">

<section class="contact py-5 bg-light" id="contact">
<div class="container">
	<div class="row">
	    <div class="col-md-12">
	        <h4>Contact</h4>
		    <hr>
	    </div>
		<div class="col-md-6">
		    <div class="address">
		        
		    <h5>Alamat:</h5>
		    <ul class="list-unstyled">
		        <li>Jl. Jend. Sudirman No.51, RT.5/RW.4, Karet Semanggi, Kecamatan Setiabudi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12930</li>
		    </ul>
		    </div>
		    <div class="email">
		    <h5>Email:</h5>
		    <ul class="list-unstyled">
		        <li>For general enquiries/untuk pertanyaan umum: <b style="color:#6f3a96;">info@alzi.or.id</b></li>
		        <li>For research-related enquiries/untuk pertanyaan sehubungan riset: <b style="color:#6f3a96;">research.alzi@gmail.com</b></li>
		    </ul>
		    </div>
		    <div class="phone">
		        <h5>Phone:</h5>
		        <ul class="list-unstyled">
		        <li>+62 816 86 3136 </li>
		    </ul>
		    </div>
		    <hr>
		</div>
		<div class="col-md-6">
		    <div class="card">
		        <div class="card-body">
		             <form>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                              <input id="Full Name" name="Full Name" placeholder="Full Name" class="form-control" type="text">
                            </div>
                            <div class="form-group col-md-6">
                              <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
                            </div>
                          </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input id="Mobile No." name="Mobile No." placeholder="Mobile No." class="form-control" required="required" type="text">
                            </div>
                            <div class="form-group col-md-6">
                                      
                                      <select id="inputState" class="form-control">
                                        <option selected>Pahami Alzheimer</option>
                                        <option> Orang Demensia</option>
                                        <option> Pendamping</option>
                                        <option> Mendukung</option>
                                        <option> Mengurangi Resiko</option>
                                        <option> Peduli demensia</option>
                                      </select>
                            </div>
                            <div class="form-group col-md-12">
                                      <textarea id="comment" name="comment" cols="40" rows="5" placeholder="Your Message"class="form-control"></textarea>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <button type="button" class="btn btn-lg" style="background-color:#6f3a96;color:#fff;">Kirim</button>
                        </div>
                    </form>
		        </div>
		    </div>
		</div>
	</div>
	
	
</div>
</section>
  
<?php
get_footer();
?>