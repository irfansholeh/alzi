<?php /* Template Name: Gallery */ ?>
<?php
get_header();
?>
    <section style="background:#f4f4f4; margin-top: 90px; color: #000">
        <div style="background-size: contain; background-repeat: no-repeat; background-position: right">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-12 col-md-6" >
                        <h3 style="padding-top: 30px; padding-bottom: 20px; ">Gallery</h3>
                    </div>
                    <div class="col-lg-6 d-none d-sm-block" style="padding-top: 10px;">
                    </div>
                </div>
            </div>
        </div>
    </section>

<section style="margin-top: 10px">
    <!------ Include the above in your HEAD tag ---------->
    <div class="container">
        <div class="row" >
    <?php
    $args = array( 'post_type' => 'gallery','order' => 'ASC','orderby'=>'title','posts_per_page'=>-1);
    $loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post();

        $setting = pods('gallery',get_the_id());
        $bio = $setting->field('bio');
        ?>
            <div class="col-md-4 col-lg-4 col-xl-4 col-12" style="margin-bottom: 15px;">
                <img src="<?= get_the_post_thumbnail_url(); ?>" alt="<?= the_title(); ?>" style="width:100%;height:100%;" class="img-thumbnail">
            </div>
    <?php
    endwhile;
    ?>
        </div>
    </div>
</section>
<br>
<?php
get_footer();
?>