<?php 
/* Template Name: single_chapter */ 
get_header();
?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

<section style="background:#f4f4f4; margin-top: 90px; color: #000">

    <div style="background-size: contain; background-repeat: no-repeat; background-position: right">
        <div class="container">
            <div class="row">
                <div class="col-lg-6" >
                    <h3 style="padding-top: 30px; padding-bottom: 20px; ">Chapter</h3>
                </div>
                <div class="col-lg-6 d-none d-sm-block" style="padding-top: 10px;">
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container" style=" padding-bottom: 120px; padding-top:50px;" >
    <div class="row">
        <?php while(have_posts()) : the_post(); ?>
        <div class="col-8"><h1 class="entry-title"><?php the_title(); ?></h1>
                    <p class="post-meta text-muted" style="padding-top:15px;font-size:14px"> <?php the_time('d/m/Y'); ?></span> | <a href="#" rel="category tag" style="color:#9756c7;">Chapter</span></a></p><article id="post-85" class="post-85 page type-page status-publish hentry">
                    <div class="entry-content">
                    <?php
                        if (has_post_thumbnail()) {
                        the_post_thumbnail('post-thumbnail', ['class' => 'fto', 'title' => 'Feature image']);
                     }else{}?>
                    <br><blockquote><p style="color:#000">
                    <?php the_content();?> 
                    <?php endwhile;?>
                    </p></blockquote>
                </div> <!-- .entry-content -->          
                </article> <!-- .et_pb_post -->
<br/><br/></div>
                            
<div class="col-lg-4" style="padding-left: 50px;">
    <?php get_template_part( 'sidebar' ); ?>
</div>
<style>
    
    .card {
    box-shadow: 0px 1px 2px 0px #e4e6e8;
    -webkit-box-shadow: 0px 1px 4px 0px #e4e6e8;
    -moz-box-shadow: 0px 1px 4px 0px #e4e6e8;
    margin:20px;
}
</style>
</div>
<br/><br/>

</div>


        </div>
</div>


    </div>

</div>

<?php  
get_footer();
?>